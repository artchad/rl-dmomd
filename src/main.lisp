;;;; dmomd.lisp
(in-package #:rl-dmomd)

;;; act
;;;-----------------------------------------------------------------------------
(defun act ()
  (process-timers)
  (unless (null *mode*)
    (mode-act *mode*)))

;;; draw
;;;-----------------------------------------------------------------------------
(defparameter *should-stop* nil)

(defun blit ()
  (with-drawing
    (clear-background +black+)
    (unless (null *mode*)
      (mode-draw *mode*))
    (when *show-fps* (draw-fps 20 20))))

(defun main ()
  (reset-modes)
  (with-window (1280 720 "DMOMD - Decent Magician Of the Moldy Dungeon (raylib)")
    ;; 4 --> resizable, 40 --> GPU vsync, 100 --> always on top
    (set-window-state (logior 40 100))
    (with-audio-device
      (set-target-fps 60) ; Set our game to run at 60 FPS
      ;; (switch-mode 'loading-mode)
      (load-resources)
      (switch-mode 'title-mode)
      (unwind-protect
           (loop until *should-stop* do
             (act)
             (blit))
        ;; (unload-resources)
        ))))

;;;----------------------------------------------------------------------------
;;; exported functions
;;;----------------------------------------------------------------------------
(defun start ()
  (setf *should-stop* nil)
  (bt:make-thread (lambda () (main)) :name "game-main"))

(defun stop () (setf *should-stop* t))
