(in-package #:rl-dmomd)

(defvar *animation-battle-demon-idle* nil)
(defvar *animation-battle-demon-hurt* nil)
(defvar *animation-battle-demon-dying* nil)
(defvar *animation-battle-demon-attack-demon-slash* nil)


(defclass demon (enemy) ()
  (:default-initargs
   :hp       100
    :hp-max  100

    :xp 0
    :xp-total 666


    :agility       6
    :strength      11
    :intelligence  4
    :initiative    5
    :dungeon-pos nil

    :battle-draw-pos (vec2 800 320)
    :physical-defense  3
    :vulnerabilities  (list :water)
    :resistances      (list :fire :lighting)))

(defmethod initialize-instance :after ((d demon) &key)
  (setf (vision-mask d) (make-mask (vision-radius d)))
  ;; (setf (current-animation d)
  ;;       *animation-dungeon-demon-idle*)
  (setf (current-animation d)
        *animation-battle-demon-idle*)
  (setf (dungeon-animations d) nil)
  (setf (battle-animations d)
        (list (cons :idle
                    *animation-battle-demon-idle*)
              (cons :hurt
                    *animation-battle-demon-hurt*)
              (cons :dying
                    *animation-battle-demon-dying*)
              (cons :attack-demon-slash
                    *animation-battle-demon-attack-demon-slash*)))

  (setf (attacks d)
        (list (cons :attack-demon-slash *attack-demon-slash*)))
  (setf (hp d) (hp-max d))
  (setf (mp d) (mp-max d)))

(defmacro make-demon (&rest args)
  `(make-instance 'demon ,@args))

(defmethod print-object ((d demon) out)
  (with-slots (hp hp-max dungeon-pos)
      d
    (print-unreadable-object (d out)
      (format out "Demon: ~A/~A at ~A" hp hp-max dungeon-pos))))


(defmethod attack :before ((d demon) (attacked can-fight) atk-keyword)
  ;; start attack animation
  (setf (current-animation d)
        (get-animation atk-keyword (battle-animations d)))
  (start-animation (current-animation d) (now)))

(defmethod attack ((d demon) (e can-fight) atk-keyword)
  (call-next-method d e atk-keyword))

(defmethod damage-for (amount (attacked demon))
  (if (eq :infinity amount)
      (setf (hp attacked) 0)
      (setf (hp attacked) (- (hp attacked) amount)))
  (if (zerop (hp attacked))
      (play-sound (get-resource '(:SND :BATTLE-MODE :DEMON :DEMON-DEATH)))
      (play-sound (get-resource '(:SND :BATTLE-MODE :DEMON :DEMON-HURT)))))

(defmethod draw ((e demon) &optional (draw-pos (battle-draw-pos e)) &key)
  (call-next-method e draw-pos))

(defmethod render-animation ((a animation) (d demon) battle-draw-pos &key)
  (unless (null a)
    (let* ((anim-pos (position-of a))
           (frame (get-frame a (now)))
           (origin (keyframe-origin frame))
           (flipped-x (keyframe-flipped-x frame))
           (flipped-y (keyframe-flipped-y frame)))
      ;; draw appropriate frame/sprite
      (draw-sprite (keyframe-image frame)
                   (v+ battle-draw-pos origin anim-pos))
      ;; after the animation has finished, change the current-animation to be the default
      (when (animation-finished-p a (now))
        (setf (attacking-p d) nil)
        (if (dead-p d)
            (setf (current-animation d)
                  *animation-battle-demon-dying*)
            (setf (current-animation d)
                  *animation-battle-demon-idle*))))))

;;;;-----------------------------------------------------------------------------
;;;; Define Animations
;;;;-----------------------------------------------------------------------------
(defun create-animations-demon ()
  "You need to make sure none of the animations is looping, except for the idle animation."

  (setf *animation-battle-demon-idle*
        (make-animation "battle-demon-idle"
                        (list
                         (list (get-resource '(:img :battle :demon :idle)) 0
                               (vec2 -170 -220) nil nil))
                        .5
                        :looped-p t
                        :position (vec2 0 0)))

  (setf *animation-battle-demon-hurt*
        (make-animation "battle-demon-hurt"
                        (list
                         (list (get-resource '(:img :battle :demon :hurt)) 0
                               (vec2 -200 -250) nil nil))
                        .4
                        :looped-p nil
                        :position (vec2 0 0)))
  (let ((x-offset -300))
   (setf *animation-battle-demon-dying*
         (make-animation
          "battle-demon-dying"
          (list
           (list (get-resource '(:img :battle :demon :dying-0)) 0
                 (vec2 x-offset -216) nil nil)
           (list (get-resource '(:img :battle :demon :dying-1)) .8
                 (vec2 (- x-offset 184) -218) nil nil)
           (list (get-resource '(:img :battle :demon :dying-2)) 1.4
                 (vec2 (- x-offset 338) -78) nil nil)
           (list (get-resource '(:img :battle :demon :dying-3)) 1.9
                 (vec2 (- x-offset 430) 100) nil nil)
           (list (get-resource '(:img :battle :demon :dying-4)) 2.6
                 (vec2 (- x-offset 502) 84) nil nil))
          5
          :looped-p nil
          :position (vec2 0 0))))

  (setf *animation-battle-demon-attack-demon-slash*
        (make-animation "battle-demon-attack-demon-slash"
                        (list
                         (list (get-resource '(:img :battle :demon :attack-slash)) 0
                               (vec2 -650 -260) nil nil))
                        .5
                        :looped-p nil
                        :position (vec2 0 0))))
