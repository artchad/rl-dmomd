(in-package #:rl-dmomd)


(defclass loading-mode (mode) ()
  (:default-initargs
   :mode-music nil))


(defmethod mode-init ((m loading-mode))
  )

(defmethod bind-buttons ((m loading-mode))
  (setf (bound-buttons m)
        (list :escape))
  (with-bind-buttons ()
    ((+key-escape+ :pressed)  (stop))))

(defmethod mode-act ((m loading-mode))
  (incf *icon-rotation* .03)
  (when *resources-loaded-p*
    (switch-mode 'title-mode)))

(defun load-dot-pos (&optional (offset 0))
  (+ (mod (+ (* (get-time) 30) offset) 16) 40))

(defmethod mode-draw ((m loading-mode))
  (let ((col (hexcolor "#851818")))
    (draw-text-ex
     (get-resource '(:fnt :quikhand))
     "Loading"
     (vec2 50 50)
     60.0
     5.0
     col)
    (draw-text-ex
     (get-resource '(:fnt :quikhand))
     "."
     (vec2 260 (load-dot-pos 4))
     60.0
     5.0
     col)
    (draw-text-ex
     (get-resource '(:fnt :quikhand))
     "."
     (vec2 280 (load-dot-pos 8))
     60.0
     5.0
     col)
    (draw-text-ex
     (get-resource '(:fnt :quikhand))
     "."
     (vec2 300 (load-dot-pos 12))
     60.0
     5.0
     col)))

(defmethod mode-cleanup ((m loading-mode)))
