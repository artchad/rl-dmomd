;;;; rl-dmomd.asd
(asdf:defsystem #:rl-dmomd
  :description "A rogue like with turn based combat."
  :author "artchad <artchad@tutanota.com>
           moldybits"
  :license  "GNU General Public License Version 3"
  :version "1.0.0"
  :depends-on (#:alexandria
               #:cl-raylib
               #:str
               #:3d-vectors
               #:mfiano.graphics.procgen.dungen)
  :serial t
  :pathname "src/"
  :components ((:file "package")
               (:file "colors")
               (:file "globals")
               (:file "math")
               (:file "utils")
               (:file "animatable")
               (:file "positionable")
               (:file "can-fight")

               (:file "timer-utils")
               (:file "position-utils")
               (:file "los-utils")
               (:file "dungeon-utils")
               (:file "animation-utils")

               (:file "mask")
               (:file "camera")
               (:file "attack")
               (:file "sprite")
               (:file "ss-sprite")
               (:file "resources")

               (:module "enemies/"
                        :serial t
                        :components
                        ((:file "enemy")
                         (:file "slime")
                         (:file "rat")
                         (:file "zombie")
                         (:file "demon")))

               (:file "player")
               (:file "items")
               (:file "interface")
               (:file "los")
               (:file "draw-utils")

               (:module "modes/"
                        :serial t
                        :components
                        ((:file "mode")
                         (:file "loading-mode")
                         (:file "title-mode")
                         (:file "rogue-mode-utils")
                         (:file "rogue-mode")
                         (:file "battle-mode-utils")
                         (:file "battle-mode")
                         (:file "game-over-mode")
                         (:file "credits-mode-utils")
                         (:file "credits-mode")))

               (:file "main")))
