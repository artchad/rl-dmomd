(in-package #:rl-dmomd)

(defun create-edge-colors (num-edges)
  (setf *edge-colors* (make-hash-table))
  (dotimes (i num-edges)
    (setf (gethash i *edge-colors*)
          (random-light-color))))

(defun show-regions (grid)
  (let ((camera-pos (vector->vec2 (camera-bottom-left *dungeon-camera*)))
        (text-offset (vec2 (- *dungeon-cell-width*  12)
                               (- *dungeon-cell-height* 14))))
    (2d-loop-over (grid x y)
      (let ((region-id (dungen::cell-region (aref *grid* x y))))
        (unless (zerop region-id)
          (draw-text-ex (get-resource '(:fnt :quikhand))
                        (format nil "~A" region-id)
                        (vec2 (floor (- (* x *dungeon-cell-width*)  (vx camera-pos)))
                            (floor (- (* y *dungeon-cell-height*) (vy camera-pos))))
                    20.0
                    1.0
                    (hexcolor "#BBBBBB")))))))

(defun draw-debug-overlay ()
  (draw-string-list
   (vec2 20 (- *window-height* 160))
   20
   +white+
   (get-resource '(:fnt :quikhand))
   20
   (list (format nil "peaceful: ~A" *peaceful*)
         (format nil "show-aggression-tiles: ~A" *show-aggression-tiles*)
         (format nil "player invincible: ~A" *player-invincible*)
         (format nil "enemies not killable: ~A" *player-can-not-kill-enemy*)
         (format nil "Level: ~A" (level *player*))
         (format nil "Experience: (~A/~A)" (xp *player*) (xp-threshold *player*))
         (format nil "Total Experience: ~A" (xp-total *player*)))))

(defun draw-poly-edges (&optional (edge-pool *edge-pool*))
  (unless (or (zerop (hash-table-count edge-pool))
              (null edge-pool))
    (maphash
     (lambda (k v)
       (with-accessors ((sx sx) (ex ex) (sy sy) (ey ey))
           v
         (let ((start-pos (position-inside-camera
                           *dungeon-camera*
                           (cell-position->pixel/bottom-left (vector sx sy))))
               (end-pos (position-inside-camera
                         *dungeon-camera*
                         (cell-position->pixel/bottom-left (vector ex ey))))
               (color (gethash k *edge-colors*)))
           (draw-line (floor  (elt start-pos 0))
                      (floor  (elt start-pos 1))
                      (floor  (elt end-pos 0))
                      (floor  (elt end-pos 1))
                      color)
           (draw-text-ex (get-resource '(:fnt :quikhand))
                         (format nil "~A" k)
                         (vec2 (floor (/ (+ (elt end-pos 0) (elt start-pos 0)) 2))
                             (floor (/ (+ (elt end-pos 1) (elt start-pos 1)) 2)))
                      16.0
                      1.0
                      color))))
     edge-pool)))

(defun gray-shade (&key (value 0)  (opacity 1))
  "Value and opacity need to be between 0 and 1."
  (vec4 value value value opacity))

(defun draw-dungeon-cell-sprite (cell-coordinates &key (opacity 1) (visible-p nil))
  "Draws the appropriate sprite"
  (let ((draw-pos (vector->vec2
                   (position-inside-camera
                    *dungeon-camera*
                    (cell-position->pixel/bottom-left cell-coordinates))))
        (cell (aref *grid* (elt cell-coordinates 0) (elt cell-coordinates 1)))
        (cell-features (cell-features cell-coordinates)))
    ;; check distance from player
    (when visible-p
      ;; we're drawing tile visible by the player
      ;; opacity ranges from 1 to .5 based on distance
      (let* ((diff (seq- (player-pos)
                            cell-coordinates))
             (abs-distance (sqrt (+ (square (elt diff 0))
                                    (square (elt diff 1))))))
        ;; (print abs-distance)
        (cond ((<= abs-distance 1.5) (setf opacity 1.0))
              ((<= abs-distance 2.5) (setf opacity .8))
              ((<= abs-distance 3.5) (setf opacity .6))
              (t (setf opacity .5)))))

    ;; sanity check
    (when (cell-outside-grid-p cell-coordinates)
      ;; when the index is out of bounds, the camera sees tiles outside of our map
      ;; we'll draw these tiles as walls
      (draw-sprite (get-resource '(:img :dungeon :wall-tile)) draw-pos)
      (return-from draw-dungeon-cell-sprite nil))
    (cond ((and (cell-has-door/horizontal-p cell) (hellfire-door-p cell))
           (draw-sprite-opacity (get-resource '(:img :dungeon :hellfire-door-h-tile)) draw-pos opacity))
          ;;------------------------------------------------------------
          ((and (cell-has-door/vertical-p cell) (hellfire-door-p cell))
           (draw-sprite-opacity (get-resource '(:img :dungeon :hellfire-door-v-tile)) draw-pos opacity))
          ;;------------------------------------------------------------
          ((cell-wall-p cell)
           (draw-sprite-opacity (get-resource '(:img :dungeon :wall-tile)) draw-pos opacity))
          ;;------------------------------------------------------------
          ((eq (car cell-features) :door/horizontal)
           (draw-sprite-opacity (get-resource '(:img :dungeon :door-h-tile)) draw-pos opacity))
          ;;------------------------------------------------------------
          ((eq (car cell-features) :door/vertical)
           (draw-sprite-opacity (get-resource '(:img :dungeon :door-v-tile)) draw-pos opacity))
          ;;------------------------------------------------------------
          (T (draw-sprite-opacity (get-resource '(:img :dungeon :floor-tile)) draw-pos opacity)))))


(defun draw-grid ()
  (let* ((cs-x (elt *dungeon-cell-size* 0))
         (cs-y (elt *dungeon-cell-size* 1))
         (ww (elt *window-size* 0))
         (wh (elt *window-size* 1))
         (dx (floor (/ ww cs-x)))
         (dy (floor (/ wh cs-y))))

    (do ((x 0 (1+ x))
         (y 0 (1+ y)))
        ((and (> x dx) (> y dy)) 'DONE)
      (draw-line (+ (* x cs-x) (/ cs-x 2))
                 0
                 (+ (* x cs-x) (/ cs-x 2))
                 wh
                 (alpha-color .5 +white+))
      (draw-line 0
                 (+ (* y cs-y) (/ cs-y 8))
                 ww
                 (+ (* y cs-y) (/ cs-y 8))
                 (alpha-color .5 +white+)))))

(defun draw-player-health-bar (&key
                        (curr-hp (hp *player*))
                        (max-hp  (hp-max *player*)))
  (let ((draw-pos (v+ *window-top-left-corner*
                                (vec2 50 75)))
        (hp-ratio (/ curr-hp max-hp))
        (max-hp-scale-factor (/ 150 max-hp))
        (hp-scale-factor (if (zerop curr-hp) 0 (/ 150 curr-hp)))
        (height 30))
    (draw-rectangle (floor (vx draw-pos))
                    (floor (vy draw-pos))
                    (floor (* max-hp  max-hp-scale-factor))
                    height
                    (hexcolor "#550000"))
    (draw-rectangle (floor (vx draw-pos))
                    (floor (vy draw-pos))
                    (floor (* (* curr-hp hp-scale-factor) hp-ratio))
                    height
                    (hexcolor "#BB0000"))
    (draw-image (v- draw-pos (vec2 21 7.5))
                (get-resource '(:img :miscellaneous :life-bar-w-200)))))

(defun draw-player-mana-bar (&key
                        (curr-mp (mp *player*))
                        (max-mp  (mp-max *player*)))
  (let ((draw-pos (v+ *window-top-left-corner*
                                (vec2 50 150)))
        (mp-ratio (/ curr-mp max-mp))
        (max-mp-scale-factor (/ 150 max-mp))
        (mp-scale-factor (if (zerop curr-mp) 0 (/ 150 curr-mp)))
        (height 30))
    (draw-rectangle (floor (vx draw-pos))
                    (floor (vy draw-pos))
                    (floor (* max-mp  max-mp-scale-factor))
                    height
                    (hexcolor "#001166"))
    (draw-rectangle (floor (vx draw-pos))
                    (floor (vy draw-pos))
                    (floor (* (* curr-mp mp-scale-factor) mp-ratio))
                    height
                    (hexcolor "#0033DD"))
    (draw-image (v- draw-pos (vec2 21 7.5))
                (get-resource '(:img :miscellaneous :life-bar-w-200)))))

(defun draw-player-xp-bar (draw-pos &key (xp (xp *player*)) (xp-threshold  (xp-threshold *player*)))
  (let* (
         (xp-ratio (/ xp xp-threshold))
         (bar-length 175)
         (xp-threshold-scale-factor (/ bar-length xp-threshold))
         (xp-scale-factor (if (zerop xp) 0 (/ bar-length xp)))
         (height 10))
    (draw-rectangle (floor (vx draw-pos))
                    (floor (vy draw-pos))
                    (floor (* xp-threshold  xp-threshold-scale-factor))
                    height
                    (hexcolor "#885522" 100))
    (draw-rectangle (floor (vx draw-pos))
                    (- (floor (vy draw-pos)) 2)
                    (floor (* (* xp xp-scale-factor) xp-ratio))
                    (+ height 4)
                    *font-color-highlight*)
    ))

(defun draw-player-bars ()
  "Draws the players health, mana."
  (draw-player-health-bar)
  (draw-player-mana-bar))


(defgeneric draw-item (i &key &allow-other-keys))

(defmethod draw-item ((item item) &key (darkness 1))
  "Draws the item at the correct pixel position.
If the item has a sprite, this sprite will be drawn.
Otherwise it will have a color instead and that color will be drawn."
  (let ((pos (vector->vec2
              (position-inside-camera
               *dungeon-camera*
               (cell-position->pixel/bottom-left (dungeon-pos item)))))
        (pos-center (vector->vec2
                     (position-inside-camera
                      *dungeon-camera*
                      (cell-position->pixel/center (dungeon-pos item)))))
        (image (item-image item)))
    (if (subtypep (type-of image) 'vec4) ; draw the color if it doesn't have a sprite
        (draw-rectangle (floor (vx pos))
                        (floor (vy pos))
                        (elt *dungeon-cell-size* 0)
                        (elt *dungeon-cell-size* 1)
                        (item-image item))
        (if (chest-p item)
            nil
            (draw-sprite (item-image item) ; draw sprite sprite if available
                         pos
                         :opacity darkness)))))

(defun draw-effects (effect-animation-list)
  (dolist (a effect-animation-list)
    (let* ((frame (get-frame a (now)))
           (origin (keyframe-origin frame))
           (flipped-x (keyframe-flipped-x frame))
           (flipped-y (keyframe-flipped-y frame))
           (position (position-of a)))
      ;; (with-pushed-canvas ())
      (draw-sprite (keyframe-image frame)
                   (v+ position origin))
      ;; after the animation has finished, change the current-animation to be the default
      (when (animation-finished-p a (now))
        (setf *effect-animations* (delete a *effect-animations* :test #'equalp))))))

(defun draw-thing (position color)
  (draw-cell position color))

(defun draw-masked-cells (origin mask color)
  (dolist (pos (relatives-to-absolutes origin (mask-to-relative mask)))
    (draw-cell pos color)))

(defun draw-cell (cell-position color)
  (let ((pos (vector->vec2
              (position-inside-camera *dungeon-camera*
                                      (cell-position->pixel/bottom-left cell-position)))))
    (draw-rectangle (floor (vx pos))
                    (floor (vy pos))
                    (elt *dungeon-cell-size* 0)
                    (elt *dungeon-cell-size* 1)
                    color)))

(defun draw-dungeon-sprite (cell-position image)
  (draw-sprite image
               (vector->vec2
                (position-inside-camera *dungeon-camera*
                                        (cell-position->pixel/bottom-left cell-position)))))

(defmethod draw-vision-circle ((p player))
  (with-slots ((r vision-radius))
      p
    (let ((pos (vector->vec2
                (position-inside-camera
                 *dungeon-camera*
                 (cell-position->pixel/center
                  (dungeon-pos p))))))
      (begin-blend-mode 1)
      (draw-circle (floor (vx pos))
                  (floor (vy pos))
                  (float (* r *dungeon-cell-width*))
                  (alpha-color .3 +raywhite+))
      (end-blend-mode))))

(defun draw-battle-enemies (&optional (battle-enemies *battle-enemies*))
  (dolist (enemy battle-enemies)
    (draw enemy (battle-draw-pos enemy))
    (draw-enemy-health-bar enemy :draw-pos (battle-draw-pos enemy))))


(defun draw-string-list (position gap color font font-height str-list &optional (active -1))
  (%draw-string-list position gap color font font-height str-list 0 active))

(defun %draw-string-list (position gap color font font-height str-list i active)
  (if (null str-list)
      'DONE
      (let* ((x-pos (floor (vx  position)))
             (y-pos (floor (vy  position)))
             (new-y-pos (+ y-pos (* i gap)))
             (text (first str-list))
             (c (if (= active i)
                        (raylib::make-rgba 255 0 0 255)
                        color))
             (tdim (measure-text-ex (get-resource '(:fnt :quikhand)) text (float font-height) 1.0))
             (indent (/ font-height 4)))
        (when (= active i)
          ;; draw red UI flare sprite
          (begin-blend-mode 8)
          (draw-texture-pro (get-resource '(:img :miscellaneous :ui-lens-flare))
                            (make-rectangle :x 0 :y 0 :width 192 :height 64)
                            (make-rectangle :x (+ x-pos indent)
                                            :y (+ y-pos (* i (vy tdim)))
                                            :width (* 2 (vx tdim))
                                            :height (* 2 (vy tdim)))
                            (vec (/ (vx tdim) 2)  (/ (vy tdim) 2))
                            0.0
                            +white+)
          (end-blend-mode))
        (draw-text-ex (get-resource '(:fnt :quikhand))
                      text
                      (vec2 (if (= active i)
                                (+ x-pos indent)
                                x-pos)
                            new-y-pos)
                      (float font-height)
                      1.0
                      c)
        ;; recursive call
        (%draw-string-list position gap color font font-height (rest str-list) (1+ i) active))))

(defun draw-player-information-menu ()
  (let ((pos (v+ *window-center* (vec2 130 -350))))
    (draw-rectangle (floor (vx pos))
                    (floor (vy pos))
                    500 700
                    (hexcolor "#1C1411")))
  ;;--------------------------------------------------------------------------------
  (let* ((font (get-resource '(:fnt :quikhand)))
         (small-font-size 22)
         (big-font-size 48)
         (pos (v+ *window-top-right-corner* (vec2 -495 80))))
    (draw-text-ex (get-resource '(:fnt :quikhand))
                  "Decent Magician:"
                  (vec2 (floor (+ (vx pos) 0))
                      (floor (- (vy pos) 60)))
                  (float big-font-size)
                  1.0
              *font-color-highlight*)
    (draw-player-xp-bar (v+ pos (vec2 -4 20)))
    (draw-string-list (v+ pos (vec2 0 40))
                      30
                      *font-color-regular*
                      font
                      small-font-size
                      (list
                       (format nil "Level: ~A (~A/~A)  -  Total XP: ~A"
                               (level *player*)
                               (xp *player*)
                               (xp-threshold *player*)
                               (xp-total *player*))
                       (format nil "Health: (~A/~A)" (hp *player*) (hp-max *player*))
                       (format nil "Mana: (~A/~A)" (mp *player*) (mp-max *player*))
                       (format nil "Strength: ~A" (strength *player*))
                       (format nil "agility: ~A" (agility *player*))
                       (format nil "Intelligence: ~A" (intelligence *player*))
                       (format nil "Initiative: ~A" (initiative *player*))
                       (format nil "Physical Defense: ~A" (physical-defense *player*))
                       (format nil "Vulnerabilities: ~{~A~^, ~}"
                               (mapcar (lambda (s) (string-capitalize (string-downcase s)))
                                       (vulnerabilities *player*)))
                       (format nil "Resistances: ~{~A~^, ~}"
                               (mapcar (lambda (s) (string-capitalize (string-downcase s)))
                                       (resistances *player*)))))

    (draw-text-ex (get-resource '(:fnt :quikhand))
                  "Inventory:"
                  (vec2 (floor (- (vx *window-top-right-corner*) 495))
                   (floor (- (vy *window-top-right-corner*) 400)))
                  (float big-font-size)
                  1.0
              *font-color-highlight*)
    (draw-string-list (v+ *window-top-right-corner* (vec2 -495 +430))
                      30
                      *font-color-regular*
                      font
                      small-font-size
                      (list (format nil "Health potions: ~A" (gethash :health-potion (inventory *player*)))
                            (format nil "Mana potions: ~A"   (gethash :mana-potion   (inventory *player*)))
                            (format nil "Hellfire Key: ~A"   (gethash :hellfire-key  (inventory *player*)))))))


(defun draw-player-vision-radius (&optional (p *player*))
  (for-cells-in-view-of (*dungeon-camera* x y cell-pos)
    (let ((pos (vector->vec2
                (position-inside-camera
                 *dungeon-camera*
                 (cell-position->pixel/center
                  cell-pos)))))
      (draw-rectangle (floor (vx pos))
                      (floor (vy pos))
                      5
                      5
                      (cond ((and (within-circle-p (vision-radius p)
                                                   (elt (seq- cell-pos (dungeon-pos p)) 0)
                                                   (elt (seq- cell-pos (dungeon-pos p)) 1))
                                  (tangential (dungeon-pos p)
                                              (vision-radius p)
                                              cell-pos))
                             +color-red+)
                            ((within-circle-p (vision-radius p)
                                              (elt (seq- cell-pos (dungeon-pos p)) 0)
                                              (elt (seq- cell-pos (dungeon-pos p)) 1))
                             +color-antiquewhite2+)
                            (t (alpha-color .7 +color-antiquewhite2+))))))
  (draw-vision-circle *player*))


(defun inside-drawing-queue-p (k)
  (dolist (d *drawing-queue*)
    (when (eq (first d) k)
      (return-from inside-drawing-queue-p t))))

(defun refresh-drawing-timer (keys)
  (dolist (k keys)
    (setf (cadr (assoc k *drawing-queue*)) (+ (now) 1.5))))


(defmethod render ((a animation) &optional (pos *window-top-left-corner*))
  (let* ((frame (get-frame a (now)))
         (origin (keyframe-origin frame))
         (flipped-x (keyframe-flipped-x frame))
         (flipped-y (keyframe-flipped-y frame)))
    (draw-sprite (keyframe-image frame)
                 (v+ origin pos))))
