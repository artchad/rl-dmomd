(in-package #:rl-dmomd)


;;;;-----------------------------------------------------------------------------
(defclass battle-mode (mode)
  ((enemies
    :accessor battle-mode-enemies
    :initarg :enemies
    :initform nil))
  (:default-initargs
   :mode-music :snd-battle-mode-music))

(defmethod create-interface ((m battle-mode))
  "Create menu and add to menus and set as active-menu."
  (with-accessors ((active-menu active-menu)
                   (menus-to-draw battle-mode-menus-to-draw)
                   (root-menu root-menu))
      m
    (let* ((menu-color *menu-default-background-color*)
           (width 180)
           (height 180)
           (origin (v- *window-bottom-left-corner* (vec2 -20 height))))
      (setf root-menu
            (setf active-menu ; set this to the active menu
                  (make-list-menu
                      ;; those are the names, the children can use
                      (list :attacks  :items  :run)
                      (list "Attacks" "Items" "Run") ; "run" will be the bottom most item
                      (list
                       (lambda ())
                       (lambda ())
                       (lambda ()
                         (progn
                           (push (list *player* nil :player-run) *attacker-queue*)
                           (setf *current-attacker* (fastest-fighter *battle-enemies*)))))
                    :origin origin
                    :width  width
                    :height height
                    :background-color menu-color
                    :name :root-menu
                    :visibility :active)))
      ;; attack menu
      (let ((attack-menu
              (setf (menu-item-child-menu (get-menu-item :attacks root-menu))
                    (make-list-menu
                        (attack-keywords *player*)
                        (if *player-invincible*
                            (list "slash" "fireball" "lightning" "deny-existence")
                            (list "slash" "fireball" "lightning"))
                        (if *player-invincible*
                            (list
                             (player-create-attack-callback :attack-slash)
                             (player-create-attack-callback :attack-fireball)
                             (player-create-attack-callback :attack-lightning)
                             (player-create-attack-callback :attack-deny-existence))
                            (list
                             (player-create-attack-callback :attack-slash)
                             (player-create-attack-callback :attack-fireball)
                             (player-create-attack-callback :attack-lightning)))
                      :origin (v+ origin (vec2 200 0))
                      :width 220
                      :height 180
                      :background-color menu-color
                      :name :attack-menu
                      :parent-menu root-menu
                      :visibility :inactive))))
        ;; create info menus for the attacks
        (let ((info-font-size 30)
              (atk-slash          (get-attack :attack-slash          *player*))
              (atk-fireball       (get-attack :attack-fireball       *player*))
              (atk-lightning      (get-attack :attack-lightning      *player*))
              (atk-deny-existence (get-attack :attack-deny-existence *player*)))
          (setf (menu-item-child-menu (get-menu-item :attack-slash attack-menu))
                (make-instance 'info-menu
                               :name :slash-info
                               :text (list (format nil "dmg: ~A"
                                                   (attack-damage atk-slash))
                                           (format nil "type: ~A"
                                                   (string-capitalize (string-downcase (attack-type atk-slash))))
                                           (format nil "Manacost: ~A" (attack-manacost atk-slash)))
                               :origin (v+ origin (vec2 430 0))
                               :width 220
                               :height 180
                               :font-size info-font-size
                               :parent-menu attack-menu
                               :background-color menu-color
                               :visibility :active))
          (setf (menu-item-child-menu (get-menu-item :attack-fireball attack-menu))
                (make-instance 'info-menu
                               :name :fireball-info
                               :text (list (format nil "dmg: ~A "
                                                   (attack-damage atk-fireball))
                                           (format nil "type: ~A"
                                                   (string-capitalize (string-downcase (attack-type atk-fireball))))
                                           (format nil "Manacost: ~A" (attack-manacost atk-fireball)))
                               :origin (v+ origin  (vec2 430 0))
                               :width 220
                               :height 180
                               :font-size info-font-size
                               :parent-menu attack-menu
                               :background-color menu-color
                               :visibility :active))
          (setf (menu-item-child-menu (get-menu-item :attack-lightning attack-menu))
                (make-instance 'info-menu
                               :name :lightning-info
                               :text (list (format nil "dmg: ~A "
                                                   (attack-damage atk-lightning))
                                           (format nil "type: ~A"
                                                   (string-capitalize
                                                    (string-downcase
                                                     (attack-type atk-lightning))))
                                           (format nil "Manacost: ~A" (attack-manacost atk-lightning)))
                               :origin (v+ origin  (vec2 430 0))
                               :width 220
                               :height 180
                               :font-size info-font-size
                               :parent-menu attack-menu
                               :background-color menu-color
                               :visibility :active))
          (when *player-invincible*
            (setf (menu-item-child-menu (get-menu-item :attack-deny-existence attack-menu))
                  (make-instance 'info-menu
                                 :name :deny-existence-info
                                 :text (list "dmg: way too much"
                                             (format nil "type: ~A"
                                                     (string-capitalize (string-downcase (attack-type atk-deny-existence))))
                                             (format nil "Manacost: ~A" (attack-manacost atk-deny-existence)))
                                 :origin (v+ origin  (vec2 430 0))
                                 :width 220
                                 :height 180
                                 :font-size info-font-size
                                 :parent-menu attack-menu
                                 :background-color menu-color
                                 :visibility :active)))))
      ;; inventory menu
      (let ((items-menu
              (setf (menu-item-child-menu (get-menu-item :items root-menu))
                    (make-list-menu
                        (list :health-potion :mana-potion)
                        (list "health-potion" "mana-potion")
                        (list (lambda ()
                                (when (and (eq *current-attacker* *player*)
                                           (not (null (gethash :health-potion (inventory *player*))))
                                           (< 0 (gethash :health-potion (inventory *player*))))
                                  (push (list *player* nil :health-potion) *attacker-queue*)
                                  (setf *current-attacker* (fastest-fighter *battle-enemies*))))
                              (lambda ()
                                (when (and (eq *current-attacker* *player*)
                                           (not (null (gethash :mana-potion (inventory *player*))))
                                           (< 0 (gethash :mana-potion (inventory *player*))))
                                  (push (list *player* nil :mana-potion) *attacker-queue*)
                                  (setf *current-attacker* (fastest-fighter *battle-enemies*)))))
                      :origin (v+ origin (vec2 200 0))
                      :width 220
                      :height 180
                      :background-color menu-color
                      :name :items-menu
                      :parent-menu root-menu
                      :visibility :inactive))))
        (setf (menu-item-child-menu (get-menu-item :health-potion items-menu))
              (make-instance 'info-menu
                             :name :health-potion-infor
                             :text (list "Restores a bit" "of your life.")
                             :origin (v+ origin (vec2 430 0))
                             :width 220
                             :height 180
                             :font-size 20
                             :parent-menu items-menu
                             :background-color menu-color
                             :visibility :active))
        (setf (menu-item-child-menu (get-menu-item :mana-potion items-menu))
              (make-instance 'info-menu
                             :name :mana-potions-info
                             :text (list "Restores a bit" "of your mana.")
                             :origin (v+ origin (vec2 430 0))
                             :width 220
                             :height 180
                             :font-size 20
                             :parent-menu items-menu
                             :background-color menu-color
                             :visibility :active))))))

(defmethod bind-buttons ((m battle-mode))
  (setf (bound-buttons m )
        (list +key-h+ +key-j+ +key-k+ +key-l+ +key-up+ +key-down+ +key-left+ +key-right+ +key-enter+ +key-tab+ +key-escape+))
    (with-bind-buttons ()
      ((+key-j+ :pressed)
       (play-sound (get-resource '(:SND :INTERFACE :SWITCH)))
       (previous-menu-item (active-menu m)))
      ((+key-down+ :pressed)
       (play-sound (get-resource '(:SND :INTERFACE :SWITCH)))
       (previous-menu-item (active-menu m)))
      ((+key-k+ :pressed)
       (play-sound (get-resource '(:SND :INTERFACE :SWITCH)))
       (next-menu-item (active-menu m)))
      ((+key-up+ :pressed)
       (play-sound (get-resource '(:SND :INTERFACE :SWITCH)))
       (next-menu-item (active-menu m)))
      ((+key-h+ :pressed)
       (play-sound (get-resource '(:SND :INTERFACE :SWITCH)))
       (select-menu-previous m))
      ((+key-left+ :pressed)
       (play-sound (get-resource '(:SND :INTERFACE :SWITCH)))
       (select-menu-previous m))
      ((+key-l+ :pressed)
       (when (and (battle-going-on-p *player* (battle-mode-enemies m))
                  (eq *current-attacker* *player*))
         (menu-call-current-menu-item (active-menu m)))
       (select-menu-next m))
      ((+key-right+ :pressed)
       (when (and (battle-going-on-p *player* (battle-mode-enemies m))
                  (eq *current-attacker* *player*))
         (menu-call-current-menu-item (active-menu m)))
       (select-menu-next m))
      ((+key-enter+ :pressed)
       (when (and (battle-going-on-p *player* (battle-mode-enemies m))
                  (eq *current-attacker* *player*))
         (menu-call-current-menu-item (active-menu m)))
       (select-menu-next m))
      ((+key-tab+ :pressed)
       (print "Opened rogue-mode-menu.")
       (setf *show-player-information-menu*
             (not *show-player-information-menu*)))
      ((+key-tab+ :released)
       (print "Closed rogue-mode-menu.")
       (setf *show-player-information-menu*
             (not *show-player-information-menu*)))
      ((+key-escape+ :pressed)
       (toggle-settings-menu))
      ((+key-f7+ :pressed)
       (setf *player-invincible* (not *player-invincible*)))
      ((+key-f8+ :pressed)
       (setf *player-can-not-kill-enemy* (not *player-can-not-kill-enemy*)))
      ((+key-f9+ :pressed)
       (setf *show-battle-guides* (not *show-battle-guides*)))))

;;;;-----------------------------------------------------------------------------
(defmethod mode-init :before ((m battle-mode))
  (setf (attacking-p *player*) nil)
  (print "~&Entered Battle-Mode!~%~%")
  (setf *in-battle* t)
  (if *in-boss-battle-p*
      (setf (mode-music m) (get-resource '(:SND :BATTLE-MODE :BOSS-BATTLE-MUSIC)))
      (setf (mode-music m) (get-resource '(:SND :BATTLE-MODE :BATTLE-MODE-MUSIC))))
  (when *in-boss-battle-p*
    (add-timer (+ (now) (- *boss-battle-enter-delay* 1.2))
               (lambda () (play-sound (get-resource '(:SND :BATTLE-MODE :DEMON :DEMON-HURT-MUCH)))))
    (add-timer (+ (now) (1- *boss-battle-enter-delay*))
               (lambda () (add-fade :duration 1 :ease :in)))
    (add-timer (+ (now) *boss-battle-enter-delay*)
               (lambda () (add-fade :duration .5 :ease :out))))
  (when *play-mode-music*
    (when *in-boss-battle-p*
      (setf *play-mode-music* nil)
      (add-timer (+ (now) (- *boss-battle-enter-delay* 1))
                 (lambda () (setf *play-mode-music* t)))
      ;; (add-timer (+ (now) (- *boss-battle-enter-delay* 1))
      ;;            (lambda () (play-mode-music m)))
      )))

(defmethod mode-init ((m battle-mode))
  ;; only do something at init time when we're fighting the boss
  (when *in-boss-battle-p*
    (setf (vy *hands-draw-pos*) (- *window-height* 400))
    (let ((dt *boss-battle-enter-delay*))
      (draw-for (dt :first-person-hands)
        `(draw-image *hands-draw-pos*
                     (get-resource '(:img :rogue-mode :first-person-hands))))
      (draw-for (dt :first-person-treasure)
        `(draw-image *window-top-left-corner*
                     (get-resource '(:img :rogue-mode :FIRST-PERSON-CHEST))))))

  (setf (battle-mode-enemies m) *battle-enemies*)
  (setf *enemies-that-have-not-attacked* *battle-enemies*)
  (setf *current-attacker* *player*)

  (setf (current-animation *player*) *animation-battle-player-idle*)
  (set-enemy-battle-animations (battle-mode-enemies m))
  (set-enemy-battle-positions (battle-mode-enemies m)))

;;;;-----------------------------------------------------------------------------
(defmethod mode-act ((m battle-mode))
  (delete-dead-enemies)
  (resolve-attack-queue)
  (switch-mode-if-ready)

  (when (and *in-boss-battle-p*
             (mode-younger-than-p (1- *boss-battle-enter-delay*)))
    (decf (vy *hands-draw-pos*)  (* .9 (delta-factor)))))

;;;;-----------------------------------------------------------------------------
(defmethod mode-draw ((m battle-mode))
  ;; draw background
  (draw-image *window-top-left-corner* (get-resource '(:img :battle-mode :battle-background-1-1280x720)))
  (draw *player*)
  (draw-battle-enemies)
  (draw-effects *effect-animations*)
  ;; HUD and UI drawing
  (draw-player-bars)
  (draw-menu (root-menu m))
  (when *show-player-information-menu*
    (draw-player-information-menu))
  (when *show-battle-guides*
    (show-battle-guides)))

;;;;-----------------------------------------------------------------------------
(defmethod cleanup ((m battle-mode))
  (when *player*
    (setf (has-attacked-p *player*) nil)
    (setf (current-animation *player*)
          (get-animation :idle (dungeon-animations *player*))))
  (clean-attributes *battle-enemies*)
  (reset-enemy-battle-positions *battle-enemies*) ; set them back to where they were before
  (setf *attacker-queue* nil)
  (setf *battle-enemies* nil)
  (setf *in-boss-battle-p* nil)
  ;; order of the two following lines is important
  (setf *in-battle* nil))
