(in-package #:rl-dmomd)

;;;; sprite-sheet sprite  ------------------------------------------------------
(defclass ss-sprite (sprite)
  ;; ss-sprite doesn't need to inherit from sprite.
  ;; I (decent-username) just added it, because I can
  ((origin
    :initarg :origin
    :accessor sprite-origin)
   (width
    :initarg :width
    :accessor sprite-width)
   (height
    :initarg :height
    :accessor sprite-height)))

(defmethod draw-sprite ((s ss-sprite) pos &key (opacity 1))
  "Draws s at pos"
  (with-accessors ((ss     sprite-image)
                   (orig   sprite-origin)
                   (width  sprite-width)
                   (height sprite-height))
      s
    (draw-texture-pro ss
                      (make-rectangle :x (vx orig) :y (vy orig) :width width :height height)
                      (make-rectangle :x (vx pos)  :y (vy pos)  :width width :height height )
                      (vec2 0 0)
                      0.0
                      (grey-color opacity))))

(defmethod draw-sprite-opacity ((s ss-sprite) pos opacity)
  "Draws s at pos"
  (with-accessors ((ss     sprite-image)
                   (orig   sprite-origin)
                   (width  sprite-width)
                   (height sprite-height))
      s
    (draw-texture-pro ss
                      (make-rectangle :x (vx orig) :y (vy orig) :width width :height height)
                      (make-rectangle :x (vx pos)  :y (vy pos)  :width width :height height )
                      (vec2 0 0)
                      0.0
                      (alpha-color opacity +white+))))

(defun make-ss-sprite (&key ss ts origin w h )
  "`ts' is the size of the tiles in the sprite sheet `ss'.
`orig' is a coordinate on the grid define by `ts'."
  (let ((x (elt origin 0))
        (y (elt origin 1)))
    (make-instance 'ss-sprite
                   :image ss
                   :origin (vec2 (* x ts) (* y ts))
                   :width  (* w ts)
                   :height (* h ts))))
