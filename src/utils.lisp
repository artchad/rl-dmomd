(in-package #:rl-dmomd)


(defun vector->vec2 (v)
  "Transforms a 2D vector into a vec2."
  (vec2 (elt v 0) (elt v 1)))

(defmacro with-percent-chance-of ((percent) &body body)
  `(when (<= (1+ (random 100)) ,percent) ,@body))

(defun kill-all-enemies ()
  (setf *enemies* nil))

(defun 2d-array-to-list (array)
  (map 'list #'identity array))

;;;; tgk wrappers
;;;;-----------------------------------------------------------------------------
(defun play-snd (keyword-list &rest keys)
  (declare (ignore keys))
  (if (null keyword-list)
      (format t "WARNING: trying to play NIL sound.")
      (play-sound (get-resource keyword-list))))

(defun draw-image (pos img &rest keys)
  (declare (ignore keys))
  (handler-case (cl-raylib:draw-texture img
                                        (floor (vx pos))
                                        (floor (vy pos))
                                        (raylib::make-rgba 255 255 255 255))
    (simple-error () nil)))

;;;;-----------------------------------------------------------------------------
(defun vec4= (v1 v2)
  (and (= (vx v1) (vx v2))
       (= (vy v1) (vy v2))
       (= (vz v1) (vz v2))
       (= (vw v1) (vw v2))))

;;;; $+ $- $* $/
;;;;-----------------------------------------------------------------------------
(defun seq+ (&rest args)
  (if (null args)
      1
      (apply #'map (class-of (first args))
             #'+
             args)))

(defun seq- (&rest args)
  (apply #'map (class-of (first args))
         #'-
         args))

(defun seq* (&rest args)
  (if (null args)
      1
      (apply #'map (class-of (first args))
             #'*
             args)))

(defun seq/ (&rest args)
  (apply #'map (class-of (first args))
         #'/
         args))

(defun seq-floor (&rest args)
  (apply #'map (class-of (first args))
         #'floor
         args))
;;;;-----------------------------------------------------------------------------
(defun keyword-list-to-string-list (kl &optional &key (lowercase-p T))
  (if lowercase-p
      (mapcar #'string-downcase
              (mapcar #'symbol-name kl))
      (mapcar #'symbol-name kl)))

(defun num-to-descending-list (num)
  "e.g. turns 5 to (5 4 3 2 1)"
  (let (result)
    (dotimes (i num)
      (push (1+ i) result))
    result))

(defun inside-arr-width (x arr)
  (let ((w (array-dimension arr 0)))
    (< x (1- w))))

(defun inside-arr-height (y arr)
  (let ((h (array-dimension arr 1)))
    (< y (1- h))))

(defmacro 2d-loop-over ((arr x-sym y-sym) &body body)
  "doc"
  `(loop :for ,x-sym :from 0 :below (array-dimension ,arr 0)
      :do (loop :for ,y-sym :from 0 :below (array-dimension ,arr 1)
             :do ,@body)))

(defmacro 3d-loop (x-sym y-sym z-sym width height depth &body body)
  `(loop :for ,x-sym :from 0 :below ,width
      :do (loop :for ,y-sym :from 0 :below ,height
             :do (loop :for ,z-sym :from 0 :below ,depth
                    :do ,@body))))

(defmacro do-hash-table ((key value hash-table &optional result) &body body)
  "Iterates through a hash table key and value just like in `dolist'."
  (let ((maphash `(maphash (lambda (,key ,value)
                             ,@body)
                           ,hash-table)))
    (if result
        `(progn ,maphash ,result)
        maphash)))

(defun insert (item lst &optional (key #'<))
  (if (null lst)
      (list item)
      (if (funcall key item (car lst))
          (cons item lst)
          (cons (car lst) (insert item (cdr lst) key)))))

(defun many-values ()
  (values
   1 2))

(defun chance-% (percent)
  (with-percent-chance-of (percent)
    (return-from chance-% t))
  nil)


(defun add-animation (animation)
  (setf (start-time-of animation) (now))
  (setf *animation-queue* (nconc *animation-queue* (list animation))))

(defun process-animation-queue ()
  (dolist (a *animation-queue*)
    (render a)
    (when (animation-finished-p a (now))
        (setf *animation-queue* (delete a *animation-queue*)))))

(defun time-ran-out (time)
  (< time (now)))

(defun center-of-internal-screen ()
  (seq/ *internal-screen-size* '(2 2)))

(defmacro with-bind-buttons (() &body body)
  (let ((result (list 'progn)))
    (dolist (b body (reverse result))
      (destructuring-bind (key state) (first b)
        (push `(bind-button ,key ,state (lambda () ,@(rest b)))
              result)))))


(defmacro draw-for ((time key) &body body)
  "Drawing things for a specified amount of time.
   Adds the statements inside the body to the `*drawing-queue*'."
  `(push (cons ,key
               (cons (+ (now) ,time)
                     (progn ,@body)))
         *drawing-queue*))

(defun process-drawing-queue ()
  "Format inside *drawing-queue*: KEY TIME DRAWING-INSTRUCTIONS"
  (dolist (d *drawing-queue*)
    (if (time-ran-out (cadr d))
        (setf *drawing-queue* (delete d *drawing-queue*))
        (eval (cddr d)))))

(defun create-reference-items (item-hash-table)
  (let ((hp-strength 30)
        (mp-strength 10))
    (setf (gethash :health-potion item-hash-table)
          (make-instance 'health-potion
                         :effect (lambda ()
                                   (unless (full-health-p *player*)
                                     (setf (hp *player*) (min (+ (hp *player*) hp-strength)
                                                              (hp-max *player*))))
                                   (draw-for (.5 :hp-gained-notification)
                                     `(draw-text-ex ,(get-resource '(:fnt :quikhand))
                                                    ,(format nil "+~AHP" hp-strength)
                                                    ,*window-center*
                                                    60.0
                                                    1.0
                                                    ,(hexcolor "#D01212"))))
                         :strength hp-strength))
    (setf (gethash :mana-potion item-hash-table)
          (make-instance 'mana-potion
                         :effect (lambda ()
                                   (unless (full-mana-p *player*)
                                     (setf (mp *player*) (min (+ (mp *player*) mp-strength)
                                                              (mp-max *player*))))
                                   (draw-for (.5 :mp-gained-notification)
                                     `(draw-text-ex ,(get-resource '(:fnt :quikhand))
                                                    ,(format nil "+~AMP" mp-strength)
                                                    ,*window-center*
                                                    60.0
                                                    1.0
                                                    ,(hexcolor "#114CDC"))))
                         :strength mp-strength))))

(defun win-game ()
  (setf *player-won* t)
  (switch-mode 'credits-mode))


(defun add-sound (resource-id &key (looped-p nil))
  (declare (ignore looped-p))
  (play-sound resource-id)
  (push resource-id *playing-sounds*))

(defun take (amount seq)
  (let ((len (length seq)))
   (cond ((> amount len) (error "Can't take ~A items from sequence with ~A items." amount len))
         (t (let (result)
              (dotimes (i amount)
                (push (pop seq) result))
              (reverse result))))))


(defun update-player-animations ()
  (create-animations-player)
  (setf (battle-animations *player*)
        (list (cons :idle
                    *animation-battle-player-idle*)
              (cons :hurt
                    *animation-battle-player-hurt*)
              (cons :dying
                    *animation-battle-player-dying*)
              (cons :attack-slash
                    *animation-battle-player-attack-slash*)
              (cons :attack-fireball
                    *animation-battle-player-attack-fireball*)
              (cons :attack-lightning
                    *animation-battle-player-attack-lightning*)
              (cons :attack-deny-existence
                    *animation-battle-player-attack-deny-existence*)
              (cons :item-health-potion
                    *animation-battle-player-item-health-potion*)
              (cons :item-mana-potion
                    *animation-battle-player-item-mana-potion*))))


(defun search-sorted (value sorted-array &key (test #'eql) (predicate #'<) (key #'identity))
  (labels ((%aref (idx)
             (aref sorted-array idx))
           (%compare (idx)
             (funcall predicate value (funcall key (%aref idx))))
           (%test (idx)
             (funcall test value (funcall key (%aref idx))))
           (%search (start end)
             (if (= start end)
                 (values nil end)
                 (let ((idx (floor (/ (+ start end) 2))))
                   (if (%test idx)
                       (values (%aref idx) idx)
                       (if (%compare idx)
                           (%search start idx)
                           (%search (1+ idx) end)))))))
    (%search 0 (length sorted-array))))

(defun delta-factor () (* (get-frame-time) 60))
