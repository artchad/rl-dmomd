(in-package #:rl-dmomd)


;;;-----------------------------------------------------------------------------
;;; Initializes the entire game
;;;-----------------------------------------------------------------------------

(defun initialize-game ()
  (hide-cursor)
  (setf *player-won* nil)
  (setf *dungeon-rooms* (make-hash-table :test #'equalp))  ; used by make-dungeon
  (setf *dungeon-empty-rooms* nil)                         ; used by make-dungeon
  (make-dungen)                                            ; make-dungeon
  (setf *dungeon-camera* (make-camera 0
                                      0
                                      (elt *internal-screen-size* 0)
                                      (elt *internal-screen-size* 1)))
  (setf *rogue-mode-items* nil)
  (setf *rogue-mode-inventory-menu* nil)
  (setf *settings-menu* nil)
  (setf *items* (make-hash-table :test #'eq))
  (create-reference-items *items*)

  (populate-poly-edges *grid*)  ; necessary for the LOS algorithm
  (setf *seen-cells* nil)
  (setf *last-safe-cell* nil)
  (setf *timers* nil)
  (setf *in-battle* nil)
  (setf *player-won* nil)
  (setf *enemies* nil)
  (create-all-animations)
  ;; player should be created here
  (setf *player*
        (make-instance 'player
                       :hp-max 70
                       :mp-max 8
                       :vision-radius 3.45
                       :vulnerabilities (list :poison)
                       :resistances (list :fire :lightning)))

  (setf *demon* (make-demon))
  ;; The order in which these spawn functions are called matters a lot!!!
  (spawn-player)
  (spawn-treasure)
  (dotimes (i 2)
      (spawn-enemies))
  ;; spawn potions
  (dotimes (i 3)
    (spawn-items))

  ;; post initialize
  (setf *loaded-resources* nil)
  (setf *number-of-registered-resources* 0)
  (switch-mode 'title-mode)
  ;; (prepare-resources)
  ;; (let ((mouse-position (vec2 0 0)))
  ;;   (defun mouse-position ()
  ;;     mouse-position)
  ;;   ;; (bind-cursor (lambda (x y) (setf mouse-position (vec2 x y))))
  ;;   )
  )

;;;; TITLE MODE
;;;;----------------------------------------------------------------------------
(defclass title-mode (mode) ()
  (:default-initargs
   :mode-music (get-resource '(:snd :title-mode :title-mode-music))))

(defmethod bind-buttons ((m title-mode))
  (setf (bound-buttons m)
        (list :h :j :k :l :down :up :right :enter))
  (with-bind-buttons ()
    ((+key-j+      :pressed)  (previous-menu-item (active-menu m)))
    ((+key-down+   :pressed)  (previous-menu-item (active-menu m)))
    ((+key-k+      :pressed)  (next-menu-item     (active-menu m)))
    ((+key-up+     :pressed)  (next-menu-item     (active-menu m)))
    ((+key-l+      :pressed)  (menu-call-current-menu-item (active-menu m)))
    ((+key-enter+  :pressed)  (menu-call-current-menu-item (active-menu m)))
    ((+key-right+  :pressed)  (menu-call-current-menu-item (active-menu m)))
    ((+key-escape+ :pressed)  (toggle-settings-menu)))
  )

(defmethod create-interface ((m title-mode))
  (create-settings-menu)
  (create-controls-menu)
  (let ((w 240)
        (h 280))
   (setf (root-menu m)
         (setf (active-menu m)
               (make-list-menu
                   '(:game  :settings  :controls  :credits  :quit)
                   '("Game" "Settings" "Controls" "Credits" "Quit")
                   (list (lambda () (format t "starting... rogue~%")
                           (initialize-game)
                           (switch-mode 'rogue-mode))
                         (lambda () (toggle-settings-menu))
                         (lambda () (toggle-controls-menu))
                         (lambda () (switch-mode 'credits-mode))
                         (lambda () (format t "quitting...~%")
                           (stop)))
                 :name "title-menu"
                 :origin (v+ *window-bottom-left-corner* (vec2 60 (-  (+ h 100))))
                 :background-color (hexcolor "#000000" 0)
                 :width w
                 :height h
                 :font-size 64
                 :visibility :active))))
  (setf (drawing-offset (root-menu m)) (vec2 0 50)))

;;;;-----------------------------------------------------------------------------
(defmethod mode-init :before ((m title-mode))
  (setf *timers* nil)
  (setf *drawing-queue* nil)
  (setf *animation-queue* nil)
  (setf *fade-list* nil)
  (setf *player-won* nil)
  (setf *in-boss-battle-p* nil)
  (setf *dont-bind-buttons* nil))

(defmethod mode-init ((m title-mode)))

;;;;-----------------------------------------------------------------------------
(defmethod mode-act ((m title-mode)))

;;;;-----------------------------------------------------------------------------
(defmethod mode-draw ((m title-mode))
  (draw-texture-pro (get-resource '(:IMG :TITLE-MODE :SPLASH-ALT-1280X720))
                    (make-rectangle :x 0 :y 0 :width 1280 :height 720)
                    (make-rectangle :x 0 :y 0
                                    :width  (get-screen-width)
                                    :height (get-screen-height))
                    (vec2 0 0)
                    0.0
                    +white+)
    (draw-menu (root-menu m)))

;;;;-----------------------------------------------------------------------------
(defmethod cleanup ((m title-mode))
  (setf *show-settings-menu* nil)
  (setf *show-controls-menu* nil))
