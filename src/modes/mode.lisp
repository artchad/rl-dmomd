(in-package #:rl-dmomd)

;;;;----------------------------------------------------------------------------
;;;; MODE
;;;;----------------------------------------------------------------------------
(defclass mode ()
  ((game :accessor mode-game)
   (init-time :reader init-time
              :documentation "initialized by (now) on mode-init")
   (music-playing :accessor music-playing
                  :initform nil
                  :documentation "Each mode needs to set this variable in the class deklaration.")
   (mode-music :accessor mode-music
                 :initarg :mode-music
                 :initform (error "Mode needs to have a default music. Use NIL for none."))
   (bound-buttons :accessor bound-buttons
                  :initarg :bound-buttons
                  :initform nil)
   (button-callbacks :accessor button-callbacks
                     :initarg :button-callbacks
                     :initform '())
   (root-menu
    :accessor root-menu
    :initarg :root-menu
    :initform nil)
   (active-menu
    :accessor active-menu
    :initarg :active-menu
    :initform nil)))

(defmethod mode-act :before ((m mode))
  (with-accessors ((music mode-music))
      m
    (unless (null music)
     (unless (is-sound-playing music)
       (play-sound music)))))

(defmethod mode-act ((m mode)))

(defmethod mode-draw ((m mode)))

(defmethod mode-init :before ((m mode))
  (add-fade :duration .5 :ease :out)
  (setf (slot-value m 'init-time) (now))
  (create-interface m)
  (unless *dont-bind-buttons*
   (bind-buttons m)))

(defmethod mode-init ((m mode)))

(defmethod create-interface (mode)
  "Creates all necessary interace objects and sets up the appropriate keybinding for using the interface."
  nil)

(defmethod bind-buttons ((m mode))
  (format t "Each mode should implement their own `bind-buttons' method. Method `bind-button' called with ~A." m))

(defmethod unbind-buttons ((m mode))
  (setf (bound-buttons m) nil)
  (setf (button-callbacks m) nil))

(defmethod select-menu-next (m)
  (print "select-menu-next called with unknown argument."))

(defmethod select-menu-previous (m)
  (print "select-menu-previous called with unknown argument."))

(defmethod switch-mode (mode-class &rest keys)
  (cleanup *mode*)  ; do everything necessary to leave the mode in a clean state
  (let ((mode (apply #'make-instance mode-class keys)))
    (setf *mode* mode)
    (mode-init *mode*)))

(defmethod play-mode-music ((m mode))
  (setf (music-playing m) t)
  (unless (null (mode-music m))
    (play-sound (mode-music m))))

(defun stop-mode-music (&optional (m *mode*))
  (setf (music-playing m) nil)
  (unless (null (mode-music m))
   (stop-sound (mode-music m))))

(defmethod elapsed-time ((m mode))
  "How much time has passed since we've entered the mode?"
  (- (now) (init-time m)))

;;; Interface

(defmethod select-menu-next :before ((m mode))
  (when (eq *current-attacker* *player*)
    (play-sound (get-resource '(:SND :INTERFACE :SWITCH)))))

(defmethod select-menu-next ((m mode))
  "Set the current menu item to the item to the right and play a button click sound."
  (with-accessors ((active-menu active-menu))
      m
    (when (and (child-menu active-menu)
               (not (info-menu-p (child-menu active-menu))))
      (setf (menu-visibility active-menu) :inactive)
      (setf active-menu (child-menu active-menu))
      (setf (menu-visibility active-menu) :active))))

(defmethod select-menu-previous :before ((m mode))
  (when (eq *current-attacker* *player*)
    (play-sound (get-resource '(:SND :INTERFACE :SWITCH)))))

(defmethod select-menu-previous ((m mode))
  "Set the current menu item to the item to the left"
  (with-accessors ((active-menu active-menu))
      m
    (when (menu-parent-menu active-menu)
      (setf (menu-visibility active-menu) :inactive)
      (setf active-menu (menu-parent-menu active-menu))
      (setf (menu-visibility active-menu) :active))))

(defmethod disable-mode-music ((m mode))
  (setf *play-mode-music* nil)
  (stop-mode-music m))

(defmethod enable-mode-music ((m mode))
  (setf *play-mode-music* t)
  (play-mode-music m))

(defmethod toggle-mode-music ((m mode))
  (if *play-mode-music* ; mode music is playing
      (disable-mode-music m)
      (enable-mode-music  m)))

(defmethod mode-handle-input ((m mode))
  (when (is-key-pressed +key-f11+)
      (toggle-fullscreen))
  (when (is-key-pressed +key-f12+)
    (toggle-show-fps))
  (dolist (c (button-callbacks m))
    (let* ((callback (cdr c))
           (x (car c))
           (key (car x))
           (state (cdr x)))
      (case state
        (:pressed
         (if (is-key-pressed key)
             (funcall callback)))
        (:released
         (if (is-key-released key)
             (funcall callback)))
        (:repeating
         (if (is-key-down key)
             (funcall callback)))))))


(defmethod mode-act :before ((m mode))
  (when (and *play-mode-music*
             (mode-music m)
             (not (is-sound-playing (mode-music m))))
    (play-sound (mode-music m)))
  (mode-handle-input m))

(defmethod mode-draw :after ((m mode))
  (process-drawing-queue)
  (process-animation-queue)
  (process-fades)
  )

;;;;----------------------------------------------------------------------------
(defmethod cleanup (m)) ; we need this one for the initial switch to 'title-mode, when *mode* is still NIL.

(defmethod cleanup :before ((m mode))
  "This method is implemented for each mode and defined specific cleanup behaviour."
  (stop-mode-music m)
  (unbind-buttons m))

(defmethod cleanup ((m mode))
  "This method is implemented for each mode and defined specific cleanup behaviour.")


(defun mode-younger-than-p (time &optional (m *mode*))
  (> (+ (init-time m) time)
     (now)))

(defun bind-button (button state callback)
  (push (cons (cons button state) callback) (button-callbacks *mode*)))

(defun reset-modes () (setf *resources-loaded-p* nil))

(defun toggle-show-fps () (setf *show-fps* (not *show-fps*)))
