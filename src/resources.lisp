(in-package #:rl-dmomd)
;;;-----------------------------------------------------------------------------
;;; Resources
;;;-----------------------------------------------------------------------------

(defparameter *resources-loaded-p* nil)
(defparameter *resource-table* (make-hash-table :test #'eq))


(defun add-resource (path object &optional (table *resource-table*))
  ;; if we're not at the end of path
  (if (and (consp path)
           (not (endp (cdr path))))
      ;; get the next ht or make one
      (let ((ht (multiple-value-bind (existing-ht foundp)
                    (gethash (car path) table)
                  (if foundp
                      existing-ht
                      (setf (gethash (car path) table)
                            (make-hash-table :test #'equalp))))))
        (add-resource (rest path) object ht))
      ;; at the end, let's get the value
      (progn
        (setf (gethash (first path)
                       table)
              object))))

(defun get-resource (path &optional (table *resource-table*))
  (if (and (consp path)
           (not (endp (cdr path))))
      (get-resource (rest path) (gethash (car path) table))
      (gethash
       ;; this check is necessary if the path is not a list
       (if (atom path) path (first path))
       table)))


(defvar *asset-path*
  (uiop:native-namestring (asdf:system-relative-pathname :rl-dmomd "assets/")))



(defun all-files-in-asset-subdir (subdir)
  (let (dirs files)
    (uiop:collect-sub*directories (concatenate 'string *asset-path* subdir)
                                  (constantly t)
                                  (constantly t)
                                  (lambda (it) (push it dirs)))
    (dolist (d dirs files)
      (dolist (f (uiop:directory-files d))
        (push (uiop:native-namestring f) files)))))

(defparameter *snd-names* (all-files-in-asset-subdir "snd/"))

(defparameter *img-names* (all-files-in-asset-subdir "img/"))

(defparameter *fnt-names* (all-files-in-asset-subdir "fnt/"))

(defun load-resources ()
  (load-assets-from-filename-list *snd-names*)
  (load-assets-from-filename-list *img-names*)
  (load-assets-from-filename-list *fnt-names*)
  (add-resources)
  ;; (setf *resources-loaded-p* t)
  )

(defun load-assets-from-filename-list (fnl)
  (dolist (fn fnl)
    (let* ((path-l (str:split "/" fn :omit-nulls t))
           (assets-dir-pos (position "assets" path-l :test #'string=))
           (relevant-elms (nthcdr (1+ assets-dir-pos) path-l))
           (k-list (map 'list (lambda (s) (intern (string-upcase (pathname-name s)) "KEYWORD")) relevant-elms)))
      (add-resource k-list (load-asset fn (first k-list))))))

(defun load-asset (path atype)
  "atype is one of :snd :img or :fnt"
  (format t "loading asset: ~A~%" path)
  (ecase atype
    (:snd (load-sound path))
    (:img (load-texture path))
    (:fnt (load-font path))
    (otherwise (error "asset type must be on of :snd :img or :fnt"))))


(defun unload-resources (&optional (table *resource-table*))
  (do-hash-table (k v table)
    (ecase k
      (:snd (unload-sounds   v) (remhash k table))
      (:img (unload-textures v) (remhash k table))
      (:fnt (unload-fonts    v) (remhash k table)))))

(defun unload-sounds (s-table)
  (if (eq (type-of s-table) 'hash-table)
      (do-hash-table (k v s-table)
        (unload-sounds v)
        (remhash k s-table))
      (unload-sound s-table)))

(defun unload-textures (t-table)
  (if (eq (type-of t-table) 'hash-table)
      (do-hash-table (k v t-table)
        (unload-textures v)
        (remhash k t-table))
      (unload-sprite t-table)))

(defun unload-fonts (f-table)
  (if (eq (type-of f-table) 'hash-table)
      (do-hash-table (k v f-table)
        (unload-fonts v)
        (remhash k f-table))
      (unload-font f-table)))

(defgeneric unload-sprite (s)
  (:method ((s sprite)) (unload-texture (sprite-image s)))
  (:method ((s ss-sprite)) (unload-texture (sprite-image s)))
  (:method ((s raylib::texture)) (unload-texture s)))

(defun add-resources ()
  (add-resource '(:img :credits :player :won-0)
                (make-sprite :image (get-resource '(:img :credits-mode :player-won-0))))
  (add-resource '(:img :credits :player :won-1)
                (make-sprite :image (get-resource '(:img :credits-mode :player-won-1))))

  ;; ;;;;-----------------------------------------------------------------------------
  ;; ;;;; tile map
  ;; ;;;;-----------------------------------------------------------------------------

  (add-resource '(:img :dungeon :floor-tile)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :tile-map))
                                :ts *top-down-ss-tile-size*
                                :origin '(0 1) :w 1 :h 1))

  (add-resource '(:img :dungeon :wall-tile)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :tile-map))
                                :ts *top-down-ss-tile-size*
                                :origin '(0 0) :w 1 :h 1))

  (add-resource '(:img :dungeon :door-h-tile)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :tile-map))
                                :ts *top-down-ss-tile-size*
                                :origin '(1 1) :w 1 :h 1))

  (add-resource '(:img :dungeon :door-v-tile)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :tile-map))
                                :ts *top-down-ss-tile-size*
                                :origin '(1 0) :w 1 :h 1))

  (add-resource '(:img :dungeon :hellfire-door-h-tile)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :tile-map))
                                :ts *top-down-ss-tile-size*
                                :origin '(2 1) :w 1 :h 1))

  (add-resource '(:img :dungeon :hellfire-door-v-tile)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :tile-map))
                                :ts *top-down-ss-tile-size*
                                :origin '(2 0) :w 1 :h 1))

  ;; ;;;;------------------------------------------------------------------------------
  ;; ;;;; items
  ;; ;;;;------------------------------------------------------------------------------

  (add-resource '(:img :dungeon :chest-closed)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :top-down-sprite-sheet-512x512))
                                :ts *top-down-ss-tile-size*
                                :origin '(0 2) :w 1 :h 1))

  (add-resource '(:img :dungeon :chest-opened-empty)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :top-down-sprite-sheet-512x512))
                                :ts *top-down-ss-tile-size*
                                :origin '(0 1) :w 1 :h 1))

  (add-resource '(:img :dungeon :chest-opened-full)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :top-down-sprite-sheet-512x512))
                                :ts *top-down-ss-tile-size*
                                :origin '(0 0) :w 1 :h 1))

  (add-resource '(:img :dungeon :potion-health)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :top-down-sprite-sheet-512x512))
                                :ts *top-down-ss-tile-size*
                                :origin '(2 2) :w 1 :h 1))

  (add-resource '(:img :dungeon :potion-mana)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :top-down-sprite-sheet-512x512))
                                :ts *top-down-ss-tile-size*
                                :origin '(2 1) :w 1 :h 1))

  (add-resource '(:img :dungeon :hellfire-key)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :top-down-sprite-sheet-512x512))
                                :ts *top-down-ss-tile-size*
                                :origin '(3 2) :w 1 :h 1))



;;;------------------------------------------------------------------------------
;;; player dungeon sprites
;;;------------------------------------------------------------------------------

  (add-resource '(:img :dungeon :player :south)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :top-down-sprite-sheet-512x512))
                                :ts *top-down-ss-tile-size*
                                :origin '(0 6) :w 1 :h 1))

  (add-resource '(:img :dungeon :player :north)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :top-down-sprite-sheet-512x512))
                                :ts *top-down-ss-tile-size*
                                :origin '(0 5) :w 1 :h 1))

  (add-resource '(:img :dungeon :player :east)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :top-down-sprite-sheet-512x512))
                                :ts *top-down-ss-tile-size*
                                :origin '(0 3) :w 1 :h 1))

  (add-resource '(:img :dungeon :player :west)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :top-down-sprite-sheet-512x512))
                                :ts *top-down-ss-tile-size*
                                :origin '(0 4) :w 1 :h 1))

;;;-----------------------------------------------------------------------------
;;; player battle sprites
;;;-----------------------------------------------------------------------------

;;; slash ----------------------------------------------------------------------

  (add-resource '(:img :battle :player :attack-slash)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :player :player-sprite-sheet-1080x1294))
                                :ts *battle-ss-tile-size*
                                :origin '(10 11) :w 4 :h 3))

  (add-resource '(:img :battle :player :attack-slash-effect)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :player :player-sprite-sheet-1080x1294))
                                :ts *battle-ss-tile-size*
                                :origin '(13 14) :w 2 :h 3))

;;; fire ------------------------------------------------------------------------

  (add-resource '(:img :battle :player :attack-fireball)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :player :player-sprite-sheet-1080x1294))
                                :ts *battle-ss-tile-size*
                                :origin '(0 8) :w 6 :h 4))

  (add-resource '(:img :battle :player :attack-fireball-effect)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :player :player-sprite-sheet-1080x1294))
                                :ts *battle-ss-tile-size*
                                :origin '(6 8) :w 4 :h 4))

;;; lightning -------------------------------------------------------------------

  (add-resource '(:img :battle :player :attack-lightning)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :player :player-sprite-sheet-1080x1294))
                                :ts *battle-ss-tile-size*
                                :origin '(0 12) :w 6 :h 4))

  (add-resource '(:img :battle :player :attack-lightning-effect)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :player :player-sprite-sheet-1080x1294))
                                :ts *battle-ss-tile-size*
                                :origin '(6 12) :w 4 :h 4))


;;; deny-existence -------------------------------------------------------------------

  (add-resource '(:img :battle :player :attack-deny-existence1)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :player :player-sprite-sheet-1080x1294))
                                :ts *battle-ss-tile-size*
                                :origin '(0 4) :w 3 :h 4))

  (add-resource '(:img :battle :player :attack-deny-existence2)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :player :player-sprite-sheet-1080x1294))
                                :ts *battle-ss-tile-size*
                                :origin '(0 0) :w 3 :h 4))

  (add-resource '(:img :battle :player :attack-deny-existence-effect1)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :player :player-sprite-sheet-1080x1294))
                                :ts *battle-ss-tile-size*
                                :origin '(3 4) :w 4 :h 4))

  (add-resource '(:img :battle :player :attack-deny-existence-effect2)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :player :player-sprite-sheet-1080x1294))
                                :ts *battle-ss-tile-size*
                                :origin '(3 0) :w 4 :h 4))

;;; items ----------------------------------------------------------------------

  (add-resource '(:img :battle :player :item-health-potion-1)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :player :player-sprite-sheet-1080x1294))
                                :ts *battle-ss-tile-size*
                                :origin '(7 4) :w 3 :h 3))

  (add-resource '(:img :battle :player :item-health-potion-2)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :player :player-sprite-sheet-1080x1294))
                                :ts *battle-ss-tile-size*
                                :origin '(7 1) :w 3 :h 3))

  (add-resource '(:img :battle :player :item-mana-potion-1)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :player :player-sprite-sheet-1080x1294))
                                :ts *battle-ss-tile-size*
                                :origin '(10 4) :w 3 :h 3))

  (add-resource '(:img :battle :player :item-mana-potion-2)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :player :player-sprite-sheet-1080x1294))
                                :ts *battle-ss-tile-size*
                                :origin '(10 1) :w 3 :h 3))


;;; other ----------------------------------------------------------------------

  (add-resource '(:img :battle :player :idle)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :player :player-sprite-sheet-1080x1294))
                                :ts *battle-ss-tile-size*
                                :origin '(10 14) :w 3 :h 3))

  (add-resource '(:img :battle :player :hurt)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :player :player-sprite-sheet-1080x1294))
                                :ts *battle-ss-tile-size*
                                :origin '(10 8) :w 4 :h 3))

  (add-resource '(:img :battle :player :dying)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :player :player-sprite-sheet-1080x1294))
                                :ts *battle-ss-tile-size*
                                :origin '(0 16) :w 4 :h 2))


;;;;------------------------------------------------------------------------------
;;;; slime dungeon sprites
;;;;------------------------------------------------------------------------------

  (add-resource '(:img :dungeon :slime :south)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :top-down-sprite-sheet-512x512))
                                :ts *top-down-ss-tile-size*
                                :origin '(1 6) :w 1 :h 1))

  (add-resource '(:img :dungeon :slime :north)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :top-down-sprite-sheet-512x512))
                                :ts *top-down-ss-tile-size*
                                :origin '(1 5) :w 1 :h 1))

  (add-resource '(:img :dungeon :slime :east)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :top-down-sprite-sheet-512x512))
                                :ts *top-down-ss-tile-size*
                                :origin '(1 4) :w 1 :h 1))

  (add-resource '(:img :dungeon :slime :west)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :top-down-sprite-sheet-512x512))
                                :ts *top-down-ss-tile-size*
                                :origin '(1 3) :w 1 :h 1))


;;;;-----------------------------------------------------------------------------
;;;; slime battle sprites
;;;;-----------------------------------------------------------------------------

;;; idle ------------------------------------------------------------

  (add-resource '(:img :battle :slime :idle)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :slime :slime-sprite-sheet-648x432))
                                :ts *battle-ss-tile-size*
                                :origin '(0 0) :w 3 :h 3))

;;; slimeball ------------------------------------------------------------

  (add-resource '(:img :battle :slime :attack-slimeball)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :slime :slime-sprite-sheet-648x432))
                                :ts *battle-ss-tile-size*
                                :origin '(3 0) :w 3 :h 3))

  (add-resource '(:img :battle :slime :attack-slimeball-effect)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :slime :slime-sprite-sheet-648x432))
                                :ts *battle-ss-tile-size*
                                :origin '(6 3) :w 3 :h 3))

;;; other ------------------------------------------------------------

  (add-resource '(:img :battle :slime :hurt)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :slime :slime-sprite-sheet-648x432))
                                :ts *battle-ss-tile-size*
                                :origin '(6 0) :w 3 :h 3))

  (add-resource '(:img :battle :slime :dying)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :slime :slime-sprite-sheet-648x432))
                                :ts *battle-ss-tile-size*
                                :origin '(0 4) :w 6 :h 2))


;;;;-----------------------------------------------------------------------------
;;;; rat dungeon sprites
;;;;-----------------------------------------------------------------------------

;;; idle ------------------------------------------------------------

  (add-resource '(:img :dungeon :rat :south)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :top-down-sprite-sheet-512x512))
                                :ts *top-down-ss-tile-size*
                                :origin '(3 6) :w 1 :h 1))

  (add-resource '(:img :dungeon :rat :north)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :top-down-sprite-sheet-512x512))
                                :ts *top-down-ss-tile-size*
                                :origin '(3 5) :w 1 :h 1))

  (add-resource '(:img :dungeon :rat :east)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :top-down-sprite-sheet-512x512))
                                :ts *top-down-ss-tile-size*
                                :origin '(3 3) :w 1 :h 1))

  (add-resource '(:img :dungeon :rat :west)
                (make-ss-sprite :ss (get-resource '(:img :rogue-mode :top-down-sprite-sheet-512x512))
                                :ts *top-down-ss-tile-size*
                                :origin '(3 4) :w 1 :h 1))


;;;;-----------------------------------------------------------------------------
;;;; rat battle sprites
;;;;-----------------------------------------------------------------------------

;;; idle ------------------------------------------------------------

  (add-resource '(:img :battle :rat :idle)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :rat :rat-sprite-sheet-1080x720))
                                :ts *battle-ss-tile-size*
                                :origin '(0 5) :w 6 :h 5))
;;; attack ------------------------------------------------------------

  (add-resource '(:img :battle :rat :attack-bite)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :rat :rat-sprite-sheet-1080x720))
                                :ts *battle-ss-tile-size*
                                :origin '(6 5) :w 7 :h 5))
;;; hurt ------------------------------------------------------------

  (add-resource '(:img :battle :rat :hurt)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :rat :rat-sprite-sheet-1080x720))
                                :ts *battle-ss-tile-size*
                                :origin '(0 0) :w 5 :h 5))

;;; dying ------------------------------------------------------------

  (add-resource '(:img :battle :rat :dying)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :rat :rat-sprite-sheet-1080x720))
                                :ts *battle-ss-tile-size*
                                :origin '(6 0) :w 7 :h 5))


  ;; ;;;-----------------------------------------------------------------------------
  ;; ;;; zombie dungeon sprites
  ;; ;;;-----------------------------------------------------------------------------
  ;; ;; idle ------------------------------------------------------------
  ;; (add-resource '(:img :dungeon :zombie :idle)
  ;;               (make-ss-sprite :ss :img-battle-zombie-sprite-sheet
  ;;                               :ts *top-down-tile-size*
  ;;                               :origin '(0 3) :w 3 :h 3))

  ;; ;;;-----------------------------------------------------------------------------
  ;; ;;; zombie battle sprites
  ;; ;;;-----------------------------------------------------------------------------
  ;; ;; idle ------------------------------------------------------------
  ;; (add-resource '(:img :battle :zombie :idle)
  ;;               (make-ss-sprite :ss :img-battle-zombie-sprite-sheet
  ;;                               :ts *top-down-tile-size*
  ;;                               :origin '(0 3) :w 3 :h 3))

  ;; ;;;-----------------------------------------------------------------------------
  ;; ;;; demon dungeon sprites
  ;; ;;;-----------------------------------------------------------------------------
  ;; ;; idle ------------------------------------------------------------
  ;; (add-resource '(:img :dungeon :demon :idle)
  ;;               (make-ss-sprite :ss :img-battle-demon-sprite-sheet
  ;;                               :ts *top-down-tile-size*
  ;;                               :origin '(0 3) :w 3 :h 3))

;;;-----------------------------------------------------------------------------
;;; demon battle sprites
;;;-----------------------------------------------------------------------------
  ;; idle ------------------------------------------------------------

  (add-resource '(:img :battle :demon :idle)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :demon :demon-sprite-sheet))
                                :ts *battle-ss-tile-size*
                                :origin '(15 18) :w 8 :h 8))


  (add-resource '(:img :battle :demon :hurt)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :demon :demon-sprite-sheet))
                                :ts *battle-ss-tile-size*
                                :origin '(14 9) :w 9 :h 8))


  (add-resource '(:img :battle :demon :attack-slash)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :demon :demon-sprite-sheet))
                                :ts *battle-ss-tile-size*
                                :origin '(9 0) :w 13 :h 8))



  (add-resource '(:img :battle :demon :dying-0)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :demon :demon-sprite-sheet))
                                :ts *battle-ss-tile-size*
                                :origin '(0 0) :w 9 :h 7))

  (add-resource '(:img :battle :demon :dying-1)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :demon :demon-sprite-sheet))
                                :ts *battle-ss-tile-size*
                                :origin '(0 8) :w 12 :h 8))

  (add-resource '(:img :battle :demon :dying-2)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :demon :demon-sprite-sheet))
                                :ts *battle-ss-tile-size*
                                :origin '(0 16) :w 14 :h 6))

  (add-resource '(:img :battle :demon :dying-3)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :demon :demon-sprite-sheet))
                                :ts *battle-ss-tile-size*
                                :origin '(0 22) :w 15 :h 4))

  (add-resource '(:img :battle :demon :dying-4)
                (make-ss-sprite :ss (get-resource '(:img :battle-mode :demon :demon-sprite-sheet))
                                :ts *battle-ss-tile-size*
                                :origin '(0 26) :w 16 :h 4))
  )
