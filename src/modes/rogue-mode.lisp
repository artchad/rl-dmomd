(in-package #:rl-dmomd)


;;;;-----------------------------------------------------------------------------
;;;; ROGUE MODE
;;;;-----------------------------------------------------------------------------
(defclass rogue-mode (mode) ()
  (:default-initargs
   :mode-music (get-resource '(:SND :ROGUE-MODE :ROGUE-MODE-MUSIC)))
  (:documentation "This is the mode you enter, once you've aggro-ed an enemy."))

(defmethod create-interface ((m rogue-mode))
  (create-inventory-menu)
  (create-settings-menu))

(defmethod bind-buttons ((m rogue-mode))
  (setf (bound-buttons m)
        (list +key-h+ +key-j+ +key-k+ +key-l+ +key-down+ +key-up+ +key-left+ +key-right+ +key-e+ +key-tab+ +key-escape+ +key-f1+ +key-f2+ +key-f3+ +key-f4+ +key-f5+ +key-f6+ +key-f7+ +key-f8+))
  (with-bind-buttons  ()
    ((+key-j+    :pressed)   (move-player :down))
    ((+key-j+    :repeating) (move-player :down))
    ((+key-down+ :pressed)   (move-player :down))
    ((+key-down+ :repeating) (move-player :down))

    ((+key-k+  :pressed)   (move-player :up))
    ((+key-k+  :repeating) (move-player :up))
    ((+key-up+ :pressed)   (move-player :up))
    ((+key-up+ :repeating) (move-player :up))

    ((+key-h+    :pressed)   (move-player :left))
    ((+key-h+    :repeating) (move-player :left))
    ((+key-left+ :pressed)   (move-player :left))
    ((+key-left+ :repeating) (move-player :left))

    ((+key-l+     :pressed)   (move-player :right))
    ((+key-l+     :repeating) (move-player :right))
    ((+key-right+ :pressed)   (move-player :right))
    ((+key-right+ :repeating) (move-player :right))

    ((+key-tab+ :pressed)
     (print "Opened rogue-mode-menu.")
     (setf *show-player-information-menu*
           (not *show-player-information-menu*)))
    ((+key-tab+ :released)
     (print "Closed rogue-mode-menu.")
     (setf *show-player-information-menu*
           (not *show-player-information-menu*)))
    ((+key-e+ :pressed) (toggle-inventory))
    ((+key-escape+ :pressed) (toggle-settings-menu))
    ((+key-f1+ :pressed) (setf *debug-mode* (not *debug-mode*)))
    ((+key-f2+ :pressed) (setf *peaceful* (not *peaceful*)))
    ((+key-f3+ :pressed) (setf *draw-poly-edges* (not *draw-poly-edges*)))
    ((+key-f4+ :pressed) (create-edge-colors *curr-edge-id*))
    ((+key-f5+ :pressed) (setf *show-regions* (not *show-regions*)))
    ((+key-f6+ :pressed) (setf *show-aggression-tiles* (not *show-aggression-tiles*)))
    ((+key-f7+ :pressed) (setf *player-invincible* (not *player-invincible*)))
    ((+key-f8+ :pressed) (setf *player-can-not-kill-enemy* (not *player-can-not-kill-enemy*)))
    ((+key-f9+ :pressed) (setf *show-player-vision-radius* (not *show-player-vision-radius*)))
    ((+key-f9+ :released) (setf *show-player-vision-radius* (not *show-player-vision-radius*)))
    ((+key-f10+ :pressed) (setf *show-grid-p* (not *show-grid-p*)))))

;;;;-----------------------------------------------------------------------------
(defmethod mode-init ((m rogue-mode))
  (pickup-items *rogue-mode-items*)
  (setf (current-animation *player*) *animation-dungeon-player-idle*))

;;;;-----------------------------------------------------------------------------
(defmethod mode-act ((m rogue-mode))
  (when *player*
   (slide-camera-towards! *dungeon-camera* (player-pos)))
  (update-chest-glow-radius))

;;;;-----------------------------------------------------------------------------
(defmethod mode-draw ((m rogue-mode))
  (unless *in-boss-battle-p*
   (rogue-draw-tiles)
   (rogue-draw-items *rogue-mode-items*)
   (rogue-draw-enemies *enemies*)
   (draw-chest)
   (when *player*
    (draw *player* (vector->vec2 (position-inside-camera
                                  *dungeon-camera*
                                  (cell-position->pixel/bottom-left (dungeon-pos *player*)))))
    (draw-player-bars))
   ;; (draw-image :img-top-down-sprite-sheet
   ;;             *window-center*)

   (when *show-player-information-menu*
     (draw-player-information-menu))
   (when *show-inventory-menu*
     (draw-inventory-menu))
   (when *show-settings-menu*
     (draw-settings-menu))
   ;; display additional information to help with debugging
   (when *debug-mode*
     (rogue-draw-debug-info))
   (when *show-grid-p*
     (draw-grid)
     (draw-text-ex (get-resource '(:fnt :quikhand))
                   (format nil "(~A/~A)" (elt (player-pos) 0) (elt (player-pos) 1))
                   (v- *window-center* (vec2 38 -8))
                   20.0 ; font size
                   5.0 ; spacing
                   +white+
                ))))

;;;;-----------------------------------------------------------------------------
(defmethod cleanup ((m rogue-mode))
  (setf *timers* nil)
  (setf *show-inventory-menu* nil)
  (setf *show-settings-menu* nil))
