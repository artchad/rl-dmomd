(in-package #:rl-dmomd)


;;;; CREDITS MODE
;;;;----------------------------------------------------------------------------
(defclass credits-mode (mode) ()
  (:default-initargs
   :mode-music nil))


(defmethod bind-buttons ((m credits-mode))
  (setf (bound-buttons m)
        (list +key-up+ +key-down+ +key-escape+))
  (with-bind-buttons  ()
    ((+key-escape+ :pressed) (switch-mode 'title-mode))))

;;;;-----------------------------------------------------------------------------
(defmethod mode-init ((m credits-mode))
  (setf *win-credits-draw-pos* (vec2 540 500))
  (setf *loose-credits-draw-pos* (vec2 400 40))
  (when *player-won*
      (let ((credits-duration 35)
            (credits-fade-out-duration 3))
        (progn
          (add-timer (+ (now) credits-duration)
                     (lambda () (switch-mode 'title-mode)))
          (add-timer (+ (now) (- credits-duration credits-fade-out-duration))
                     (lambda () (add-fade :duration credits-fade-out-duration :ease :in)))

          ;; fade out to black
          (add-timer (+ (now) (- *dungeon-leave-animation-duration* 1))
                     (lambda () (add-fade :duration 1 :ease :in)))

          (draw-for (*dungeon-leave-animation-duration* :win-credits-background)
            `(draw-image *window-top-left-corner*
                         (get-resource '(:img :credits-mode :WIN-CREDITS-1))))
          (add-animation *animation-credits-player-won*)
          ;; fade in to credits roll
          (add-timer (+ (now) *dungeon-leave-animation-duration*)
                     (lambda () (add-fade :duration 1 :ease :out)))))))

;;;;-----------------------------------------------------------------------------
(defmethod mode-act ((m credits-mode)))

;;;;-----------------------------------------------------------------------------
(defmethod mode-draw ((m credits-mode))
  (if *player-won*
      (draw-win-credits)
      (draw-loose-credits)))

;;;;-----------------------------------------------------------------------------
(defmethod cleanup ((m credits-mode)))
