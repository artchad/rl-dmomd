(in-package #:rl-dmomd)


(defclass sprite ()
  ((image :initarg :image :accessor sprite-image)))

(defun make-sprite (&key image)
  "`image' is a image defined by define-image."
  (make-instance 'sprite :image image))

(defgeneric draw-sprite (s pos &key &allow-other-keys))

(defmethod draw-sprite ((s sprite) pos &key (opacity 1))
  (draw-texture (sprite-image s) (floor (vx pos)) (floor (vy pos)) :white))
