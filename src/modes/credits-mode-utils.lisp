(in-package #:rl-dmomd)

(defparameter *credits-text* '("Programming - decent-username (aka artchad),"
                               "                   moldybits"
                               ""
                               ""
                               "Images - decent-username (aka artchad)"
                               ""
                               ""
                               "Title Music - Public Domain"
                               "Dungeon Music - Brandon Morris"
                               "Battle Music - PlayOnLoop"
                               "Boss Battle Music - Scrabbit"
                               "Sound Effects - Kenney & others"
                               ""
                               ""
                               "Main Font - Joanne Taylor"
                               "Consola Font - wmk69"
                               ""
                               ""
                               "       SPECIAL THANKS TO"
                               ""
                               "    MFIANO - dungen library"
                               ""
                               "                 AND"
                               ""
                               "            #LISPGAMES"))

(defun draw-win-credits ()
  (draw-image *window-top-left-corner*
              (get-resource '(:img :credits-mode :win-credits-2-bg)))
  (draw-string-list *win-credits-draw-pos*
                    20 ; gap
                    (hexcolor "#272C43")
                    (get-resource '(:fnt :consolamono-bold))
                    30 ; font height
                    *credits-text*)
  (draw-image *window-top-left-corner*  (get-resource '(:img :credits-mode :win-credits-2-fg)))
  (decf (vy *win-credits-draw-pos*) (* (delta-factor) .45)))

(defun draw-loose-credits ()
  (draw-string-list *loose-credits-draw-pos*
                    26 ; gap
                    (hexcolor "#851818")
                    (get-resource '(:fnt :consolamono-bold))
                    30 ; font height
                    *credits-text*))
