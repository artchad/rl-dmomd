(in-package #:rl-dmomd)

(defvar *animation-dungeon-slime-idle* nil)
(defvar *animation-battle-slime-attack-slimeball* nil)
(defvar *animation-battle-slime-attack-slimeball-effect* nil)
(defvar *animation-battle-slime-idle* nil)
(defvar *animation-battle-slime-hurt* nil)
(defvar *animation-battle-slime-dying* nil)


(defclass slime (enemy)
  ((vision-mask
    :initform (make-mask (enemy-vision-radius 'slime))))
  (:default-initargs
   :hp 80
    :hp-max 80
    :xp 0
    :xp-total 300
    :agility      1
    :strength     7
    :intelligence 1
    :initiative    1
    :physical-defense 4
    :vulnerabilities (list :fire)
    :resistances     (list :poison)))

(defmethod print-object ((s slime) out)
  (with-slots (hp hp-max dungeon-pos)
      s
    (print-unreadable-object (s out)
      (format out "Slime: ~A/~A at ~A" hp hp-max dungeon-pos))))

(defmethod initialize-instance :after ((s slime) &key)
  (setf (current-animation s)
        *animation-dungeon-slime-idle*)
  (setf (dungeon-animations s)
        (list (cons :idle *animation-dungeon-slime-idle*)))
  (setf (battle-animations s)
        (list (cons :idle *animation-battle-slime-idle*)
              (cons :hurt *animation-battle-slime-hurt*)
              (cons :dying *animation-battle-slime-dying*)
              (cons :idle *animation-battle-slime-attack-slimeball*)
              (cons :attack-slimeball *animation-battle-slime-attack-slimeball*)))
  (setf (battle-attack-effect-animations s)
        (list (cons :attack-slimeball *animation-battle-slime-attack-slimeball-effect*)))
  (setf (attacks s)
        (list (cons :attack-slimeball *attack-slimeball*))))

(defmethod attack ((s slime) (attacked can-fight) atk-keyword)
  (call-next-method s attacked atk-keyword))

(defmethod attack :after ((s slime) (attacked can-fight) atk-keyword))

(defmethod draw ((e slime) &optional (draw-pos (battle-draw-pos e)) &key)
  (call-next-method))

(defmethod render-animation ((a animation) (s slime) window-pos &key (opacity 1))
  (unless (null a)
    (let* ((frame (get-frame a (now)))
           (origin (keyframe-origin frame))
           (flipped-x (keyframe-flipped-x frame))
           (flipped-y (keyframe-flipped-y frame))
           (position (position-of a)))
      ;; (with-pushed-canvas ())
      (draw-sprite (keyframe-image frame)
                   (v+ origin window-pos))
      ;; after the animation has finished, change the current-animation to be the default
      (when (animation-finished-p a (now))
        (cond ((eql (type-of *mode*) 'rogue-mode)
               (setf (current-animation s)
                     (get-animation :idle (dungeon-animations s))))

              ((eql (type-of *mode*) 'battle-mode)
               (setf (attacking-p s) nil)
               (unless (dead-p s)
                 (setf (current-animation s)
                       (get-animation :idle (battle-animations s)))))))
      )))

(defmethod attack :before ((s slime) (attacked can-fight) atk-keyword)
    ;; start attack animation
  (setf (current-animation s)
        (get-animation atk-keyword (battle-animations s)))
  (start-animation (current-animation s) (now))
  ;; start attack effect animation
  (push (get-animation atk-keyword (battle-attack-effect-animations s)) *effect-animations*)
  (start-animation (get-animation atk-keyword (battle-attack-effect-animations s)) (now)))

;;;;-----------------------------------------------------------------------------
;;;; Define Animations
;;;;-----------------------------------------------------------------------------
(defun create-animations-slime ()
  ;; slime dungeon animations

  (setf *animation-dungeon-slime-idle*
        (make-animation "dungeon-slime-idle"
                        (list
                         (list (get-resource '(:img :dungeon :slime :south)) 0
                               (vec2 0 0) nil nil))
                        1 :looped-p nil))
    ;; slime battle animations

  (setf *animation-battle-slime-attack-slimeball*
        (make-animation "battle-slime-attack-slimeball"
                        (list
                         (list (get-resource '(:img :battle :slime :attack-slimeball)) 0
                               (vec2 0 0) nil nil))
                        .5 :looped-p nil))

  (setf *animation-battle-slime-attack-slimeball-effect*
        (make-animation "battle-slime-attack-slimeball-effect"
                        (list
                         (list (get-resource '(:img :battle :slime :attack-slimeball-effect)) 0
                               (vec2 -30 100) nil nil))
                        .35
                        :looped-p nil
                        :position *battle-player-default-pos*))

  (setf *animation-battle-slime-idle*
        (make-animation "battle-slime-idle"
                        (list
                         (list (get-resource '(:img :battle :slime :idle)) 0
                               (vec2 -30 10) nil nil))
                        1 :looped-p nil))

  (setf *animation-battle-slime-hurt*
        (make-animation "battle-slime-hurt"
                        (list
                         (list (get-resource '(:img :battle :slime :hurt)) 0
                               (vec2 0 0) nil nil))
                        .4 :looped-p nil))

  (setf *animation-battle-slime-dying*
        (make-animation "battle-slime-dying"
                        (list
                         (list (get-resource '(:img :battle :slime :dying)) 0
                               (vec2 -130 100) nil nil))
                        *enemy-death-anim-duration* :looped-p nil)))

(defmethod attack-enemies (atk-keyword enemy-list (s slime))
  "Attacks each enemy inside `enemy-list'"
  (dolist (e enemy-list)
    (attack s e atk-keyword)))

(defmethod draw-aggression-mask ((s slime) &optional (opacity .15))
  (dolist (aggression-pos (relatives-to-absolutes (dungeon-pos s)
                                                  (mask-to-relative (vision-mask s))))
    ;; loop over all coordinates that could potentially be drawn
    (begin-blend-mode 2)
    (when (valid-aggression-tile-pos-p aggression-pos s)  ; check if it should be drawn
      (draw-cell aggression-pos (alpha-color opacity (hexcolor "#227722"))))
    (end-blend-mode)))

(defun spawn-slimes (positions)
  (unless positions
    (error "~A shouldn't be empty." positions))
  (dolist (pos positions)
    (push (make-instance 'slime
                         :dungeon-pos pos
                         :room-id (region-of pos))
          *enemies*)))
