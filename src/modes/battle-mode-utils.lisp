(in-package #:rl-dmomd)


(defun menu-active-p (m)
  (unless (null m)
    (eq :active (menu-visibility m))))

(defun menu-inactive-p (m)
  (unless (null m)
    (eq :inactive (menu-visibility m))))

(defun menu-hidden-p (m)
  (unless (null m)
    (eq :hidden (menu-visibility m))))

(defun damage-fuzz (n)
  (* n (1+ (/ (random 10) 10))))

(defun critical-factor ()
  ;; 10% chance to crit
  (if (< (random 100) 10)
      2
      1))

(defun battle-run ()
  (setf (attacking-p *player*) t)
  (teleport-to! *last-safe-cell*)
  (let ((fade-duration .5))
    (add-fade :duration fade-duration :ease :in)
    (add-timer (+ (now) fade-duration)
               (lambda () (switch-mode 'rogue-mode)))))

(defun fastest-fighter (fighter-list)
  "Determines the fastest `can-fight' with `has-attacked-p' equal to nil."
  (let (fastest)
    (dolist (curr-fighter fighter-list)
      (cond ((or (null fastest))
             (setf fastest curr-fighter))
            ((> (initiative curr-fighter)
                (initiative fastest))
             (setf fastest curr-fighter))
            ((= (initiative curr-fighter)
                (initiative fastest))
             (with-percent-chance-of (50)
               (setf fastest curr-fighter)))))
    fastest))

(defun fighters-that-have-not-attacked-yet (fighter-list)
  (if (null fighter-list)
      (format t "~&All fighters have already had their turn.~&")
   (let (result)
     (dolist (curr-fighter fighter-list)
       (when (not (has-attacked-p curr-fighter))
         (push curr-fighter result)))
     result)))

(defun start-new-round ()
  ;; the attacker-queue should already be empty
  (format t "~&~%Started a new round.~%---------------------~%")
  (setf (has-attacked-p *player*) nil)
  (setf *attacker-queue* nil)
  (setf *current-attacker* *player*)
  ;; (setf *current-attacker* (fastest-fighter (cons *player* *battle-enemies*)))
  (setf *enemies-that-have-not-attacked* *battle-enemies*)

  (dolist (e *battle-enemies*)
    (setf (has-attacked-p e) nil)
    (setf (attacking-p e) nil)))

(defun someone-attacking-p (can-fighters)
  "`can-fighters' is a list of `can-fight' objects."
  (find-if (lambda (can-fighter) (attacking-p can-fighter))
           can-fighters))

(defun noone-attacking-p (can-fighters)
  (not (someone-attacking-p can-fighters)))

(defun battle-going-on-p (p enemies)
  (and (noone-attacking-p (cons p enemies))
       (not (dead-p p))
       (and enemies (some-enemies-alive-p enemies))))

(defun some-enemies-alive-p (enemies)
  (find-if-not (lambda (x) (dead-p x)) enemies))

(defun everyone-has-attacked-p ()
  (and (null *current-attacker*)
       (null *attacker-queue*)))

(defun some-have-not-yet-attacked-p ()
  (and (null *current-attacker*)
       (not (null *attacker-queue*))))

(defun not-everyone-in-attacker-queue-yet-p ()
  (not (null *current-attacker*)))

(defun clean-attributes (enemy-list)
  (mapc
   (lambda (e)
     (setf (has-attacked-p e) nil)
     (if (eq e *demon*)
         nil
         (setf (current-animation e) (get-animation :idle (dungeon-animations e)))))
   enemy-list))

(defun enemies-can-attack ()
  nil)

;; (defmethod player-select-target (target-list)
;;   "allows the player to interactively chose an enemy.
;; Returns: selected enemey."
;;   (push (list *player* *battle-enemies* :attack-slash)
;;         *battle-enemies*))

(defmacro sort-attacker-list (l attribute)
  "Sort the elements of `l' by `attribute'.
Bigger means better, means closer to the start of the list `l'."
  `(sort ,l
         (lambda (x y)
           (if (> (,attribute x) (,attribute y))
               t
               nil))
         :key #'car))

(defun randomize-attack-order (attackers)
  "`attackers' is a list of `can-fight' objects. Sorted by `initiative'.
Randomize the order of all fighters with the same `initiative'."
  (let* (result
         (buckets (make-hash-table)))
    (dolist (a attackers)
      (push a (gethash (initiative (first a)) buckets)))
    (do-hash-table (k bucket buckets result)
      (setf (gethash k buckets) (a:shuffle bucket
                                 ))
      (dolist (b bucket)
        (setf result (insert b result (lambda (x y) (> (initiative (first x)) (initiative (first y))))))))))

(defun crit? (agi)
  (>= agi (1+ (random 100))))


(defmacro player-create-attack-callback (atk-keyword)
  `(lambda ()
     (when (eq *current-attacker* *player*)
       (if (and (enough-mana-p *player* ,atk-keyword))
           (progn
             (push (list *player* *battle-enemies* ,atk-keyword) *attacker-queue*)
             (setf *current-attacker* (fastest-fighter *battle-enemies*)))
           ;; else
           (draw-for (.5 :not-enough-mana-notification)
             `(draw-text-ex ,(get-resource '(:fnt :quikhand))
                            "Not enough mana."
                            ,(vec2 (floor (+ (vx *window-center*) -100))
                                   (floor (+ (vy *window-center*) 50)))
                            40.0
                            1.0
                            ,+color-violetred1+))))))

(defun set-enemy-battle-animations (enemy-list)
  "Set enemy animations to battle animations."
  (dolist (enemy enemy-list)
    (cond ((eq (type-of enemy) 'slime)
           (setf (current-animation enemy) *animation-battle-slime-idle*))
          ((eq (type-of enemy) 'rat)
           (setf (current-animation enemy) *animation-battle-rat-idle*))
          ;; ((eq (type-of enemy) 'zombie)
          ;;  (setf (current-animation enemy) *animation-battle-zombie-idle*))
          ((eq (type-of enemy) 'demon)
           (setf (current-animation enemy) *animation-battle-demon-idle*)))))

(defun set-enemy-battle-positions (enemy-list &optional (x-off 100) (y-off 50))
  (let* ((offset 0))
    (dolist (enemy enemy-list)
      (setf (battle-draw-pos enemy)
            (v+ (battle-draw-pos enemy)
                (vec2 (* offset x-off)
                      (* offset y-off))))
      (incf offset))))


(defun reset-enemy-battle-positions (animatable-list)
  (dolist (cf animatable-list)
    (setf (battle-draw-pos cf) *battle-enemy-default-pos*)))


(defun player-can-kill-enemy-p ()
  (not *player-can-not-kill-enemy*))


(defun resolve-attack-queue ()
  (when (battle-going-on-p *player* *battle-enemies*)
    (cond
      ;;----------------------------------------------------------------------
      ((everyone-has-attacked-p)
       (start-new-round))
      ;;----------------------------------------------------------------------
      ((some-have-not-yet-attacked-p)
       ;; resolve attacks
       (symbol-macrolet ((atk-entry (first *attacker-queue*))
                         (attacker    (first atk-entry))
                         (targets     (second atk-entry))
                         (atk-keyword (third atk-entry)))
         (cond
           ;;----------------------------------------------------------------------
           ((eq atk-keyword :player-run)
            (battle-try-run *player* *battle-enemies*)
            (setf *attacker-queue* (rest *attacker-queue*)))
           ;;----------------------------------------------------------------------
           ((eq atk-keyword :health-potion)
            (format t "Player consumed health potion.")
            (play-sound (item-sound (gethash :health-potion *items*)))
            (setf (current-animation *player*) (get-animation :item-health-potion (battle-animations *player*)))
            (start-animation (current-animation *player*) (now))
            (setf (attacking-p *player*) t)
            (take-player-item :health-potion)
            (funcall (item-effect (gethash :health-potion *items*)))
            (setf *attacker-queue* (rest *attacker-queue*)))
           ;;----------------------------------------------------------------------
           ((eq atk-keyword :mana-potion)
            (format t "Player consumed mana potion.")
            (play-sound (item-sound (gethash :mana-potion *items*)))
            (setf (current-animation *player*) (get-animation :item-mana-potion (battle-animations *player*)))
            (start-animation (current-animation *player*) (now))
            (setf (attacking-p *player*) t)
            (take-player-item :mana-potion)
            (funcall (item-effect (gethash :mana-potion *items*)))
            (setf *attacker-queue* (rest *attacker-queue*)))
           ;;----------------------------------------------------------------------
           (t
            (when (and (ready-to-attack-p attacker)
                       (not (attacking-p attacker)))
              (if (null targets)
                  (wait attacker)
                  ;; when the attacks are being resolved, the attacker could die, before he gets his turn
                  (if (can-attack-p attacker atk-keyword)
                      (progn
                        (attack attacker (first targets) atk-keyword)
                         (pop targets))
                      (progn (setf (has-attacked-p attacker) t)
                             (pop targets)))))))))
      ;;----------------------------------------------------------------------
      ((and (not-everyone-in-attacker-queue-yet-p)
            (not (eq *current-attacker* *player*))) ; *current-attacker* is not NIL
       ;; We still need to add enemies to the `attacker-queue'.
       (add-next-attacker)))))


(defun add-next-attacker ()
  (let ((enemy-atk-keyword (enemy-choose-attack-keyword *current-attacker*)))
    (push (list *current-attacker* (list *player*) enemy-atk-keyword) *attacker-queue*)
    (setf *current-attacker*
          (fastest-fighter (setf *enemies-that-have-not-attacked*
                                 (remove *current-attacker*
                                         *enemies-that-have-not-attacked*))))
    (when (null *enemies-that-have-not-attacked*)
      (setf *attacker-queue* (randomize-attack-order *attacker-queue*)))))

(defun delete-dead-enemies ()
  "Deletes enemy if he was killed and the dying animation has finished.
   Sets *player-won* to t if the demon has been slain."
  (dolist (e *battle-enemies*)
    (when (and (dead-p e)
               (player-can-kill-enemy-p)
               (equalp (current-animation e)
                       (get-animation :dying (battle-animations e)))
               (animation-finished-p (current-animation e) (now)))
      (when (eq e *demon*)
        (setf *player-won* t))
      ;; remove the enemy
      (setf *enemies*        (delete e *enemies*))
      (setf *battle-enemies* (delete e *battle-enemies*))
      (when (and (no-rats-left-p)
                 (not (has-item-p :hellfire-key *player*)))
        (spawn-hellfire-key (dungeon-pos e))))))

(defun switch-mode-if-ready ()
  (when *in-battle*
    (cond
      ;;----------------------------------------------------------
      ;; switch to credits-mode when demon has been slain
      ((dead-p *demon*)
       ;; demon death animation length is 5 seconds
       (let ((delay 4)
             (fade-duration 1))
         (setf *player-won* t)
         (setf *in-battle* nil)
         (add-timer (+ (now) delay)
                    (lambda () (add-fade :duration fade-duration :ease :in)))
         (add-timer (+ (now) delay fade-duration)
                    (lambda () (switch-mode 'credits-mode)))))
      ;;----------------------------------------------------------
      ;; switch to game-over-mode if player died
      ((and (noone-attacking-p (cons *player* *battle-enemies*))
            (dead-p *player*)
            (not *player-invincible*)
            (player-animation-is-p :dying)
            (animation-finished-p (current-animation *player*) (now)))
       (switch-mode 'game-over-mode))
      ;;----------------------------------------------------------
      ;; change back to rogue mode if player dead or everything is killed
      ((and (all-enemies-dead-p *battle-enemies*)
            (not *player-can-not-kill-enemy*))
       (setf *in-battle* nil)
       (let ((delay 1)
             (fade-duration 0.6))
         (add-timer (+ (now) delay)
                    (lambda () (add-fade :duration fade-duration :ease :in)))
         (add-timer (+ (now) (+ delay fade-duration))
                    (lambda () (switch-mode 'rogue-mode))))))))

(defun show-battle-guides ()
  (let ((idx 0))
   (dolist (e *battle-enemies*)
     (let* ((draw-pos (battle-draw-pos e))
            (x (floor (vx draw-pos)))
            (y (floor (vy draw-pos))))
       (draw-rectangle x y 4 4 +blue+)
       (draw-text-ex (get-resource '(:fnt :quikhand))
                     (format nil "~A: ~A (~A/~A)" idx (class-name (class-of e)) (hp e) (hp-max e))
                     (vec2 (+ x 18) y)
                     16.0
                     1.0
                     +blue+))
     (incf idx))))
