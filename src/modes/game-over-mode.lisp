(in-package #:rl-dmomd)


;;;; GAME-OVER MODE
;;;;----------------------------------------------------------------------------
(defclass game-over-mode (mode) ()
  (:default-initargs
   :mode-music nil))


;;;;-----------------------------------------------------------------------------
(defmethod mode-init ((m game-over-mode))
  (add-timer (+ (now) 5) (lambda () (add-fade :duration 1.5 :ease :in)))
  (play-sound (get-resource '(:SND :MISC :JINGLE-LOSE))))

;;;;-----------------------------------------------------------------------------
(defmethod mode-act ((m game-over-mode))
  (when (< 6.5 (elapsed-time m))
    (switch-mode 'title-mode)))

;;;-----------------------------------------------------------------------------
(defmethod mode-draw ((m game-over-mode))
  (draw-sprite (get-resource '(:img :battle :player :dying))
               (vector->vec2 #(500 175)))
  (draw-text-ex (get-resource '(:fnt :quikhand))
                "GAME-OVER"
                (vec2 480 420)
             80.0
             1.0
             +red+))

;;;;-----------------------------------------------------------------------------
(defmethod cleanup ((m game-over-mode)))
